<?php

namespace App\Jobs;


use App\FormData;


use PDF;
use Carbon\Carbon;


use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class ReportUpdater implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $form_data;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FormData $form_data)
    {
        $this->form_data = $form_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $form_data = $this->form_data;

        if ($form_data->report_path)
        {
            $pdf_relative_path  = $form_data->report_path;
        }
        else
        {
            $pdf_relative_path  = 'storage/reports/'.str_slug($form_data->site_name).'_'.str_slug(Carbon::now()->toDateString()).'_'.str_slug(Carbon::now()->toTimeString()).'.pdf';
        }

        $pdf_file_path      = public_path($pdf_relative_path);

        $report_data        = json_decode($form_data->parameters, true);

        $pdf                = PDF::loadView('pages.report', compact(['report_data']));

        // Save the PDF to filesystem
        $pdf->save($pdf_file_path);

        return TRUE;
    }
}

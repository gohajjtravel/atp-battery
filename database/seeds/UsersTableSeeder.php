<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Cviebrock\EloquentSluggable\Services\SlugService;


use App\Role;
use App\User;
use App\Surveyor;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        /*
        // Administrator
        $role = Role::where('name', 'administrator')->first();
        $user           = new User;
        $user->name     = 'The Admin Man';
        $user->email    = 'r00t4bl3@gmail.com';
        $user->password = bcrypt('asdfasdf');
        $user->save();
        $user->attachRole($role);


        // Surveyor
        $role = Role::where('name', 'staff')->first();
        $user           = new User;
        $user->name     = 'John Staff';
        $user->email    = 'cheap.programming@gmail.com';
        $user->password = bcrypt('asdfasdf');
        $user->save();
        $user->attachRole($role);


        $role = Role::where('name', 'staff')->first();
        $user           = new User;
        $user->name     = 'Satriyo';
        $user->email    = 'satriyopamungkas@gmail.com';
        $user->password = bcrypt('asdfasdf');
        $user->save();
        $user->attachRole($role);

        $role = Role::where('name', 'staff')->first();
        $user           = new User;
        $user->name     = 'Yusuf Wisnu Brata';
        $user->email    = 'yusufwisnubrata82@gmail.com';
        $user->password = bcrypt('asdfasdf');
        $user->save();
        $user->attachRole($role);

        */

        // /*

        $role = Role::where('name', 'staff')->first();
        $user           = new User;
        $user->name     = 'RPT Jambi';
        $user->email    = strtolower('Rptjambi@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Lampung';
        $user->email    = strtolower('Rptlampung@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Pekanbaru';
        $user->email    = strtolower('Rptpekanbaru@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Palembang';
        $user->email    = strtolower('Rptpalembang@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Padang';
        $user->email    = strtolower('Rptpadang@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Jakarta';
        $user->email    = strtolower('Rptjakarta@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Kalselteng';
        $user->email    = strtolower('Rptkalselteng@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Kaltim';
        $user->email    = strtolower('Rptkaltim@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Sulawesi';
        $user->email    = strtolower('Rptsulawesi@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Jabar';
        $user->email    = strtolower('Rptjabar@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Jateng';
        $user->email    = strtolower('Rptjateng@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Jatim';
        $user->email    = strtolower('Rptjatim@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Lombok';
        $user->email    = strtolower('Rptlombok@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Kalbar Kalut';
        $user->email    = strtolower('Rptkalbarkalut@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Bali';
        $user->email    = strtolower('Balirpt@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        $user           = new User;
        $user->name     = 'RPT Medan';
        $user->email    = strtolower('Medanrpt@gmail.com');
        $user->password = bcrypt('raptek2018');
        $user->save();
        $user->attachRole($role);

        // */



    }
}

<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


use Carbon\Carbon;
use Jenssegers\Optimus\Optimus;


class FormData extends Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'user_id', 'parameters', 'user',
    ];


    protected $appends  = [
        'hashed_id',
        'staff_name',
        'submitted_at',
        'nbwo_ewo_number',
        'old_battery_count',
        'old_battery_cage',
        'install_on_same_rack',
        'is_old_cable_enough',
        'rectifier_module_count',
        'battery_fuse_fail_alarm',
        'oss_power',
    ];

    public function getHashedIdAttribute()
    {
        return app(Optimus::class)->encode($this->id);
    }

    public function getStaffNameAttribute()
    {
        // return null;
        return $this->user ? $this->user->name : "USER_DELETED";
    }

    public function getSubmittedAtAttribute()
    {
        return $this->created_at->toDateTimeString();
    }

    public function getNbwoEwoNumberAttribute()
    {
        $parameters = json_decode($this->parameters);
        return empty($parameters->NBWO_EWO_NUMBER) ? null : $parameters->NBWO_EWO_NUMBER;
    }

    public function getOldBatteryCountAttribute()
    {
        $parameters = json_decode($this->parameters);
        return empty($parameters->OLD_BATTERY->QTY) ? null : $parameters->OLD_BATTERY->QTY;
    }

    public function getOldBatteryCageAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "Tidak Ada";
        // $return_value = null;
        if ( ! empty($parameters->OLD_BATTERY->CAGE) )
        {
            if ( $parameters->OLD_BATTERY->CAGE == 1 )
            {
                $return_value = "Ada";
            }
        }
        return $return_value;
    }

    public function getInstallOnSameRackAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "Beda Rack";
        // $return_value = null;
        if ( ! empty($parameters->INSTALL_ON_SAME_RACK) )
        {
            if ( $parameters->INSTALL_ON_SAME_RACK == 1 )
            {
                $return_value = "Ya";
            }
        }
        return $return_value;
    }

    public function getIsOldCableEnoughAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "Tidak";
        if ( ! empty($parameters->IS_OLD_CABLE_ENOUGH) )
        {
            if ( $parameters->IS_OLD_CABLE_ENOUGH == 1 )
            {
                $return_value = "Ya";
            }
        }
        return $return_value;
    }


    public function getBatteryFuseFailAlarmAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "Tidak";
        if ( ! empty($parameters->ALARM_BATTERY_FUSE_FAIL) )
        {
            $return_value = "Ya";
        }
        return $return_value;
    }


    public function getOssPowerAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "Tidak";
        if ( ! empty($parameters->NMM_OSS_POWER) )
        {
            $return_value = "Ya";
        }
        return $return_value;
    }


    public function getRectifierModuleCountAttribute()
    {
        $parameters = json_decode($this->parameters);
        $return_value = "N/A";
        if ( ! empty($parameters->RECTIFIER_MODULE_COUNT) )
        {
            $return_value = $parameters->RECTIFIER_MODULE_COUNT;
        }
        return $return_value;
    }


    public function getCreatedAtAttribute($value)
    {
        // $the_value = Carbon::createFromFormat('Y-m-d H:i:s', $value, 'UTC');
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->setTimeZone('Asia/Jakarta');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

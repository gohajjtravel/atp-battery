<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


use Jenssegers\Optimus\Optimus;
use Cviebrock\EloquentSluggable\Sluggable;


class User extends Authenticatable
{

    use Notifiable;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends  = ['hashed_id', 'role_display_name', 'dashboard_access'];

    public function sluggable()
    {
        return [
            'username' => [
                'source' => 'name',
                'separator' => '.',
            ]
        ];
    }

    public function getHashedIdAttribute()
    {
        return app(Optimus::class)->encode($this->id);
    }


    public function getRoleDisplayNameAttribute()
    {
        return $this->roles->first()->display_name;
    }

    public function getDashboardAccessAttribute()
    {
        return ($this->is_active == 1) ? "Yes" : "No";
    }


    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


    public function attachRole($role)
    {
        if (is_object($role)) {
            return $this->roles()->attach($role->id);
        }
        return false;
    }


    public function detachRole($role)
    {
        if (is_object($role)) {
            return $this->roles()->detach($role->id);
        }
        return false;
    }


    public function hasRole($name)
    {
        foreach($this->roles as $role)
        {
            if ($role->name === $name) return true;
        }

        return false;
    }

    public function form_datas()
    {
        return $this->hasMany('App\FormData');
    }

    // Declare event handlers
    protected static function boot()
    {
        parent::boot();

        static::deleting( function($user) {
            $user->form_datas()->delete();
        });
    }
}

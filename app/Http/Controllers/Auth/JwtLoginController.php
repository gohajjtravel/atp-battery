<?php

namespace App\Http\Controllers\Auth;


use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class JwtLoginController extends Controller
{
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials    = $request->only('email', 'password');
        // $log            = new Logger('debug');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                Log::info('User trying to login with invalid credentials.', $credentials);
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            Log::info('User could not create token.', $credentials);
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        Log::info('User successfully logged in.', $credentials, $token);
        return response()->json(compact('token'));
    }

    public function refreshToken()
    {
        $token          = JWTAuth::getToken();
        $refreshedToken = JWTAuth::refresh($token);
        Log::info('Token refreshed successfully.');
        return response()->json(['success' => TRUE, 'token' => $refreshedToken]);
    }

}

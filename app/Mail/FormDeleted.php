<?php

namespace App\Mail;


use App\FormData;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormDeleted extends Mailable
{
    use Queueable, SerializesModels;

    public $form_data;

    public $tries = 1;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FormData $form_data)
    {
        $this->form_data = $form_data;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Form Deleted')
                    ->view('emails.form_deleted');
    }
}

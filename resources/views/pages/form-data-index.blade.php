@extends('layouts.dashboard')



@section('page-title')
@lang('pages.form-data.title')
@endsection



@section('page-style')
<!-- DataTables -->
<!-- <link rel="stylesheet" href="{{ asset('vendors/datatables/jquery.dataTables.min.css') }}"> -->
<link rel="stylesheet" href="{{ asset('vendors/datatables/dataTables.bootstrap.css') }}">
@endsection



@section('body-class')
 sidebar-mini
@endsection



@section('page-content')
<div class="wrapper">


    @include('layouts.header')


    @include('layouts.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @lang('pages.form-data.title')
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> @lang('pages.common.home')</a></li>
                <li class="active">@lang('pages.form-data.title')</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @include('layouts.flash')

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-condensed table-hover" id="users-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>@lang('pages.form-data.staff_name')</th>
                                        <th>@lang('pages.form-data.site_name')</th>
                                        <th>@lang('pages.form-data.submitted_at')</th>
                                        <th>@lang('pages.common.actions')</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.footer')

</div>
<!-- ./wrapper -->
@endsection



@section('modals')
<div class="modal fade" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.form-data.delete-modal.title')</h4>
            </div>
            <div class="modal-body">
                <div class="callout callout-danger">
                    <i class="fa fa-exclamation-triangle "></i>&nbsp;&nbsp;@lang('pages.common.delete-message')
                </div>
                <form method="post" class="form-horizontal" action="{{ route('form-data.destroy') }}">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.staff_name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.staff_name')" name="staff_name" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.site_name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.site_name')" name="site_name" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.submitted_at')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.submitted_at')" name="submitted_at" readonly="readonly">
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.delete-modal.close-button')</button>
                <button type="button" class="btn btn-danger submit-form-button">@lang('pages.common.delete-modal.delete-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<div class="modal fade" id="approveModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.form-data.approve-modal.title')</h4>
            </div>
            <div class="modal-body">
                <div class="callout callout-success">
                    <i class="fa fa-exclamation-triangle "></i>&nbsp;&nbsp;@lang('pages.form-data.approve-message')
                </div>
                <form method="post" class="form-horizontal" action="{{ route('form-data.approve') }}">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.staff_name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.staff_name')" name="staff_name" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.site_name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.site_name')" name="site_name" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.form-data.submitted_at')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="@lang('pages.form-data.submitted_at')" name="submitted_at" readonly="readonly">
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.delete-modal.close-button')</button>
                <button type="button" class="btn btn-success submit-form-button">@lang('pages.form-data.approve-modal.approve-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection



@section('javascripts')
<!-- DataTables -->
<script src="{{ asset('vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
$(function () {

    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        order: [[ 3, "desc" ]],
        ajax: '{!! route('form-data.data') !!}',
        columns: [
            { data: 'hashed_id', sortable: false },
            { data: 'staff_name' },
            { data: 'site_name' },
            { data: 'submitted_at' },
            { data: 'actions', name: 'actions', sortable: false }
        ]
    });

    $('#addModal, #approveModal, #deleteModal').on('click', '.submit-form-button', function() {
        var theForm = $(this).parents('.modal-content').find('form');
        // TODO: need to validate the form
        theForm.submit();
    });

    $('#approveModal, #deleteModal').on('show.bs.modal', function (event) {
        if (event.namespace == "bs.modal") {

            var button = $(event.relatedTarget);
            var modal = $(this);
            var itemId = button.data('id');

            $.get('{{ route("form-data.show") }}', {
                id: itemId
            })
            .done(function( data ) {
                modal.find('.form-control').each(function() {
                    $(this).val('');
                });

                modal.find('input[name="id"]').val(itemId);
                modal.find('input[name="staff_name"]').val(data.staff_name);
                modal.find('input[name="site_name"]').val(data.site_name);
                modal.find('input[name="submitted_at"]').val(data.submitted_at);
            })
            .fail(function() {
            });
        }
    });
});

</script>
@endsection

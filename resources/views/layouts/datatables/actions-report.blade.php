<a role="button" class="btn btn-primary btn-xs edit-data-button" href="{{ route('report.show', $hashed_id) }}">Edit</a>
<a role="button" class="btn btn-primary btn-xs show-data-button" href="{{ asset($report_path) }}" target="_blank">View</a>
@if ($is_approved != 1)
<button type="button" class="btn btn-success btn-xs approve-data-button" data-id="{{ $hashed_id }}" data-toggle="modal" data-target="#approveModal">Approve</button>
<button type="button" class="btn btn-danger btn-xs delete-data-button" data-id="{{ $hashed_id }}" data-toggle="modal" data-target="#deleteModal">Delete</button>
@endif

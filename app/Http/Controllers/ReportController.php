<?php

namespace App\Http\Controllers;


use App\FormData;


use Jenssegers\Optimus\Optimus;
use Yajra\Datatables\Datatables;


// use DB;
use Illuminate\Http\Request;



use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;



class ReportController extends Controller
{

    public function __construct(Request $request, Optimus $optimus)
    {
        $this->middleware('auth');
        $this->request  = $request;
        $this->optimus  = $optimus;
    }



    public function index()
    {
        return view('pages.report-index');
        // return $users;
    }



    // Show report data (allowing user to update report)
    public function show($id)
    {
        $form_data          = FormData::findOrFail($this->optimus->decode($id));
        $report_data        = json_decode($form_data->parameters, true);
        return view('pages.report-edit', compact('form_data', 'report_data'));

    }


    public function getData()
    {
        return Datatables::of(FormData::whereNotNull('report_path')->get())
                    ->whitelist(['staff_name', 'site_name'])
                    ->addColumn('actions', 'layouts.datatables.actions-report')
                    ->rawColumns(['actions'])
                    ->make(true);
    }


    public function exportExcel()
    {
        // Populate the data
        $form_data = FormData::whereNotNull('report_path')->get();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Aji Aini Hanif')
            ->setLastModifiedBy('Aji Aini Hanif')
            ->setTitle('Battery ATP Report')
            ->setSubject('Battery ATP Report')
            ->setDescription('Report generated via database.')
            ->setKeywords('Battery ATP Report apkappa')
            ->setCategory('Battery ATP Report');

        // Add some data
        $spreadsheet->setActiveSheetIndex(0);

        // $spreadsheet->getActiveSheet()
        //     ->setCellValue('A1', 'Hello')
        //     ->setCellValue('B2', 'world!')
        //     ->setCellValue('C1', 'Hello')
        //     ->setCellValue('D2', 'world!');

        // Column names
        $spreadsheet->getActiveSheet()->setCellValue('A1', 'No.');
        $spreadsheet->getActiveSheet()->setCellValue('B1', 'Site Name');
        $spreadsheet->getActiveSheet()->setCellValue('C1', 'Site Address');
        $spreadsheet->getActiveSheet()->setCellValue('D1', 'Site ID');
        $spreadsheet->getActiveSheet()->setCellValue('E1', 'Staff Name');
        $spreadsheet->getActiveSheet()->setCellValue('F1', 'Installer Name');
        $spreadsheet->getActiveSheet()->setCellValue('G1', 'EWO/NBWO');
        $spreadsheet->getActiveSheet()->setCellValue('H1', 'Submit Date');
        $spreadsheet->getActiveSheet()->setCellValue('I1', 'Merk Baterai Lama');
        $spreadsheet->getActiveSheet()->setCellValue('J1', 'Jumlah Baterai Lama');
        $spreadsheet->getActiveSheet()->setCellValue('K1', 'Kerangkeng Baterai Lama');
        $spreadsheet->getActiveSheet()->setCellValue('L1', 'Kabel Lama Cukup');
        $spreadsheet->getActiveSheet()->setCellValue('M1', 'Jumlah Modul Rectifier');
        $spreadsheet->getActiveSheet()->setCellValue('N1', 'Merk Baterai Baru');
        $spreadsheet->getActiveSheet()->setCellValue('O1', 'Jumlah Baterai Baru');
        $spreadsheet->getActiveSheet()->setCellValue('P1', 'Serial Number Baterai Baru');
        $spreadsheet->getActiveSheet()->setCellValue('Q1', 'Install di Rectifier Lama');
        $spreadsheet->getActiveSheet()->setCellValue('R1', 'Tegangan Input');
        $spreadsheet->getActiveSheet()->setCellValue('S1', 'Jumlah Fase Input');
        $spreadsheet->getActiveSheet()->setCellValue('T1', 'Tegangan Float');
        $spreadsheet->getActiveSheet()->setCellValue('U1', 'Alarm Battery Fuse Fail');
        $spreadsheet->getActiveSheet()->setCellValue('V1', 'Terhubung ke OSS Power');
        $spreadsheet->getActiveSheet()->setCellValue('W1', 'Load Current');
        $spreadsheet->getActiveSheet()->setCellValue('X1', 'Temperature');
        $spreadsheet->getActiveSheet()->setCellValue('Y1', 'URL Form ATP');

        foreach ($form_data as $index => $data_row)
        {
            $row = $index + 1;
            $d_row = $index + 2;
            $parameter_data = json_decode($data_row->parameters);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$d_row, $row);
            $spreadsheet->getActiveSheet()->setCellValue('B'.$d_row, $data_row->site_name);
            $spreadsheet->getActiveSheet()->setCellValue('C'.$d_row, isset($parameter_data->SITE_ADDRESS) ? $parameter_data->SITE_ADDRESS : '');
            $spreadsheet->getActiveSheet()->setCellValue('D'.$d_row, isset($parameter_data->SITE_ID) ? $parameter_data->SITE_ID : '');
            $spreadsheet->getActiveSheet()->setCellValue('E'.$d_row, $data_row->staff_name);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$d_row, isset($parameter_data->INSTALLER_NAME) ? $parameter_data->INSTALLER_NAME : '');
            $spreadsheet->getActiveSheet()->setCellValue('G'.$d_row, $data_row->nbwo_ewo_number);
            $spreadsheet->getActiveSheet()->setCellValue('H'.$d_row, $data_row->submitted_at);
            $spreadsheet->getActiveSheet()->setCellValue('I'.$d_row, isset($parameter_data->OLD_BATTERY->BRAND) ? $parameter_data->OLD_BATTERY->BRAND : '');
            $spreadsheet->getActiveSheet()->setCellValue('J'.$d_row, $data_row->old_battery_count);
            $spreadsheet->getActiveSheet()->setCellValue('K'.$d_row, $data_row->old_battery_cage);
            $spreadsheet->getActiveSheet()->setCellValue('L'.$d_row, $data_row->is_old_cable_enough);
            $spreadsheet->getActiveSheet()->setCellValue('M'.$d_row, $data_row->rectifier_module_count);
            $spreadsheet->getActiveSheet()->setCellValue('N'.$d_row, isset($parameter_data->BATTERY_BRAND) ? $parameter_data->BATTERY_BRAND : '');
            $spreadsheet->getActiveSheet()->setCellValue('O'.$d_row, isset($parameter_data->BATTERY_QTY) ? $parameter_data->BATTERY_QTY : '');
            $spreadsheet->getActiveSheet()->setCellValue('P'.$d_row, isset($parameter_data->BATTERY_SN) ? $parameter_data->BATTERY_SN : '');
            $spreadsheet->getActiveSheet()->setCellValue('Q'.$d_row, $data_row->install_on_same_rack);
            $spreadsheet->getActiveSheet()->setCellValue('R'.$d_row, isset($parameter_data->AC_INPUT) ? $parameter_data->AC_INPUT : '');
            $spreadsheet->getActiveSheet()->setCellValue('S'.$d_row, isset($parameter_data->INPUT_PHASE) ? $parameter_data->INPUT_PHASE : '');
            $spreadsheet->getActiveSheet()->setCellValue('T'.$d_row, isset($parameter_data->FLOAT_VOLTAGE) ? $parameter_data->FLOAT_VOLTAGE : '');
            $spreadsheet->getActiveSheet()->setCellValue('U'.$d_row, $data_row->battery_fuse_fail_alarm);
            $spreadsheet->getActiveSheet()->setCellValue('V'.$d_row, $data_row->oss_power);
            $spreadsheet->getActiveSheet()->setCellValue('W'.$d_row, isset($parameter_data->LOAD_CURRENT) ? $parameter_data->LOAD_CURRENT : '');
            $spreadsheet->getActiveSheet()->setCellValue('X'.$d_row, isset($parameter_data->TEMPERATURE) ? $parameter_data->TEMPERATURE : '');
            $spreadsheet->getActiveSheet()->setCellValue('Y'.$d_row, $data_row->report_path ? url($data_row->report_path) : '');

        }

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report');

        // Resize width
        // $spreadsheet->getActiveSheet()->getColumnDimension('B:I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);


        // Apply style
        $spreadsheet->getActiveSheet()->getStyle('A1:Y1')->applyFromArray(
            [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => 'FF000000'],
                        ],
                    ],
                    'fill' => [
                        // 'fillType' => Fill::FILL_GRADIENT_LINEAR,
                        'fillType' => Fill::FILL_SOLID,
                        // 'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFE0E0E0',
                        ],
                        // 'endColor' => [
                        //     'argb' => 'FFFFFFFF',
                        // ],
                    ],
                ]
        );


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="report_'.gmdate('Ymd').'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;

    }


    // public function getDataOld()
    // {
    //     $all_reports = DB::table('form_datas')
    //                     ->select(DB::raw('count(id) as y, date(created_at) as x'))
    //                     ->groupBy('x')
    //                     ->get();
    //     $approved_reports = DB::table('form_datas')
    //                     ->select(DB::raw('count(id) as y, date(created_at) as x'))
    //                     ->where('is_approved', 1)
    //                     ->groupBy('x')
    //                     ->get();
    //
    //     return ['all' => $all_reports, 'approved' => $approved_reports];
    // }
}

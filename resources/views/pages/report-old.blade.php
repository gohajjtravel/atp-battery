<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <style>
    @media print {
        @page { size: A4 portrait; margin: 0; }
        body, .wrap { padding: 0; background: none; border: none; margin: 0; }
        .document { width: 180mm; height: 260mm; margin: 15mm; line-height: 1; }
        .document div { width: 100%; height: 98%; border: 1px solid red; }
    }

    /*
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: normal;
        src: local('Source Sans Pro'), local('SourceSansPro-Regular'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/ODelI1aHBYDBqgeIAH2zlNzbP97U9sKh0jjxbPbfOKg.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: bold;
        src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/toadOcfmlt9b38dHJxOBGLsbIrGiHa6JIepkyt5c0A0.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: italic;
        font-weight: normal;
        src: local('Source Sans Pro Italic'), local('SourceSansPro-It'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/M2Jd71oPJhLKp0zdtTvoM0DauxaEVho0aInXGvhmB4k.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: italic;
        font-weight: bold;
        src: local('Source Sans Pro Bold Italic'), local('SourceSansPro-BoldIt'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/fpTVHK8qsXbIeTHTrnQH6Edtd7Dq2ZflsctMEexj2lw.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Arial Narrow';
        font-style: normal;
        font-weight: normal;
        src: url(pdf/fonts/ArialNarrowRegular.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Arial Narrow';
        font-style: normal;
        font-weight: bold;
        src: url(pdf/fonts/ArialNarrowBold.ttf) format('truetype');
    }
    */

    @page {
        margin: 10px 20px;
    }


    body {
        font-family: 'Source Sans Pro';
        font-size: 0.75em;
        line-height: 0.9em;
    }

    .checkbox {
        width: 10px;
        height: 10px;
        padding: 0 4px;
    }

    .table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        page-break-inside: auto;
    }

    .table td {
        padding: 3px;
        line-height: 1.1em;
        vertical-align: top;
    }

    .table th {
        line-height: 1.1em;
    }

    .table-center-heading th {
        text-align: center;
    }

    .table-bordered {
        border: 1px solid #333;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #333;
    }

    .header,
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
        font-size: 0.8em;
    }
    .footer .pagenum:before {
          content: counter(page);
    }

    .footer .pagecount:before {
          content: counter(pages);
    }

    .header {
        top: 0px;
    }
    .footer {
        bottom: 70px;
    }

    .page-content {
        width: 100%;
        position: absolute;
        top: 130px;
        bottom: 150px;
    }

        /*p.header {
        padding-top: 10px;
        padding-bottom: -14px;
        font-size: 1em;
        font-weight: bold;
    }

    table .narrow-font {
        padding: 2px;
    }

    .narrow-font {
        font-family: 'Arial Narrow';
        font-size: 1em;
    }


    table > thead> th {
        font-family: "Arial Narrow";
    }

    table.treatment-list > tbody > tr > td:first-child {
        text-align: center;
    }*/

    h1,
    h2,
    h3,
    h4,
    h5,
    .text-center {
        text-align: center;
        /* font-family: "Arial Narrow"; */
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        margin: 10px 0;
    }

    .checkbox {
        width: 10px;
        height: 10px;
        padding: 0 4px;
    }

    .checkbox-inline {
        float:right;
        /*margin-left: 20px;*/
    }

    .table-header {
        padding-top: 20px;
        padding-bottom: 20px;
        border: 1px dashed #333;
    }

    .field-header {
        background-color: #eee;
        width: 30%;
    }

    .field-content span {
        padding-right: 10px;
    }

    .logo-header {
        width: 80px;
    }

    .doc-image {
        max-width: 180px;
        max-height: 185px;
    }

    .align-left {
        text-align: left;
    }

    .align-right {
        text-align: right;
    }

    .field-center {
        text-align: center;
    }

    .photo-grid-2 {
        max-width: 260px;
    }

    .photo-grid-4 {
        max-width: 160px;
    }

    table p {
        padding: 0;
        margin: 0;
        line-height: 0.9;
    }

    /*tr.title {
        background-color: #ccc;
    }

    tr.subtitle {
        background-color: #eee;
        font-weight: bold;
    }

    span.emphasis {
        border-bottom: 1px solid #000;
    }*/
    .page-break {
        page-break-after: always;
    }

    </style>



</head>

<body>

    <div class="header">
        <table class="table table-header">
            <tbody>
                <tr>
                    <td><img src="img/logo-only.png" class="logo-header"></td>
                    <td class="align-right"><img src="img/xl_logo.png" class="logo-header"></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="footer">
        <table class="table table-bordered">
            <tr>
                <td class="field-center">
                    <p>ATP Battery</p>
                </td>
                <td class="field-center">
                    <p>Footer text goes here</p>
                </td>
                <td class="field-center">
                    <p>Page <span class="pagenum"></span>

                    </p>
                </td>
            </tr>

        </table>
    </div>



    <div class="page-content">

        <h1>Berita Acara Uji Terima</h1>
        <p>Pada hari {{ $pages->page_1->hari or '' }} tanggal {{ $pages->page_1->tanggal or '' }} bulan {{ $pages->page_1->bulan or '' }} tahun {{ $pages->page_1->tahun or '' }} telah diuji terimakan pekerjaan {{ $pages->page_1->pekerjaan or '' }} dengan keterangan sebagai berikut:</p>
        <h3>Site Identification</h3>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th width="30%">Vendor</th>
                    <td>PT Rajawali Prima Teknik</td>
                </tr>
                <tr>
                    <th>Site Name/Number</th>
                    <td>{{ $pages->page_1->site_name_number or '' }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $pages->page_1->site_address or '' }}</td>
                </tr>
                <tr>
                    <th>Area</th>
                    <td>{{ $pages->page_1->area or '' }}</td>
                </tr>
                <tr>
                    <th>NBWO/EWO Number</th>
                    <td>{{ $pages->page_1->nbwo_number or '' }}</td>
                </tr>
                <tr>
                    <th>PO. Number(line Item)</th>
                    <td>{{ $pages->page_1->po_number or '' }}</td>
                </tr>
                <tr>
                    <th>Reservation Number (if Any)</th>
                    <td>{{ $pages->page_1->reservation_number or '' }}</td>
                </tr>
            </tbody>
        </table>
        <div class="page-break"></div>

        <h3>Material / Service List And Labelling</h3>
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th rowspan="2">NO&nbsp;</th>
                    <th rowspan="2">SAP Material/service Number&nbsp;</th>
                    <th rowspan="2">Material Service Description</th>
                    <th rowspan="2">Brand/Type</th>
                    <th rowspan="2">Qty</th>
                    <th colspan="2">Result</th>
                </tr>
                <tr>
                    <th>OK</th>
                    <th>NOK</th>
                </tr>
                @if ($pages->page_2->material)
                    <tr>
                        @foreach ($pages->page_2->material as $material)
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $material->sap_material_number }}</td>
                        <td>{{ $material->material_service_description }}</td>
                        <td>{{ $material->brand_type }}</td>
                        <td>{{ $material->qty }}</td>
                        @if ($material->result == 'OK')
                        <td><img src="img/checkbox-checked.png" class="checkbox"> &nbsp;</td>
                        <td><img src="img/checkbox-default.png" class="checkbox"> &nbsp;</td>
                        @else
                        <td><img src="img/checkbox-default.png" class="checkbox"> &nbsp;</td>
                        <td><img src="img/checkbox-checked.png" class="checkbox"> &nbsp;</td>
                        @endif
                        @endforeach
                    </tr>
                @endif
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td width="34%">Checked By</td>
                    <td width="33%">Approved By</td>
                    <td width="33%">Contractor</td>
                </tr>
                <tr>
                    <td>PT Huawei MSP</td>
                    <td>PT XL Axiata</td>
                    <td>PT Rajawali Prima Teknik</td>
                </tr>
                <tr>
                    <td>
                        <p>Signature</p>
                        <p>&nbsp;</p>
                        <p>Date &amp; Time</p>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>

        <div class="page-break"></div>


        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Breaker Number<br>(sesuai dengan label)</th>
                    <th>Kapasitas Breaker</th>
                    <th>Qty</th>
                    <th>Nama Equipment</th>
                </tr>
                @if ($pages->page_3->breaker)
                    @foreach ($pages->page_3->breaker as $breaker)
                    <tr>
                        <td>{{ $breaker->number }}</td>
                        <td>{{ $breaker->capacity }}</td>
                        <td>{{ $breaker->qty }}</td>
                        <td>{{ $breaker->equipment_name }}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>


        <div class="page-break"></div>


        <h3>Acceptance Test Battery</h3>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th width="30%">Date</th>
                    <td>{{ $pages->page_4->date or '' }}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td>PT XL Axiata</td>
                </tr>
                <tr>
                    <th>Site Name</th>
                    <td>{{ $pages->page_4->site_name or '' }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $pages->page_4->site_address or '' }}</td>
                </tr>
            </tbody>
        </table>
        <ol style="list-style-type: none;">
            <li><h4>A. Rectifier Power System</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td colspan="2">Access Power System</td>
                            <td colspan="2">S/N: {{ $pages->page_4->access_power_system_sn or '' }}</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td colspan="2">Subrack</td>
                            <td colspan="2">S/N: {{ $pages->page_4->subrack_sn or '' }}</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td colspan="2">Supervisory Module</td>
                            <td colspan="2">S/N: {{ $pages->page_4->supervisory_module_sn or '' }}</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Rectifier Modul Type</td>
                            <td colspan="3">{{ $pages->page_4->rectifier_module_type or '' }}</td>
                        </tr>
                        <tr>
                            <td>4.a</td>
                            <td>Rectifier 1</td>
                            <td>S/N: {{ $pages->page_4->rectifier_1_sn or '' }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>4.b</td>
                            <td>Rectifier 2</td>
                            <td>S/N: {{ $pages->page_4->rectifier_2_sn or '' }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>4.c</td>
                            <td>Rectifier 3</td>
                            <td>S/N: {{ $pages->page_4->rectifier_3_sn or '' }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>4.d</td>
                            <td>Rectifier 4</td>
                            <td>S/N: {{ $pages->page_4->rectifier_4_sn or '' }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>4.e</td>
                            <td>Rectifier 5</td>
                            <td>S/N: {{ $pages->page_4->rectifier_5_sn or '' }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>AC Input</td>
                            <td>{{ $pages->page_4->ac_input_voltage or '' }} VAC</td>
                            <td>{{ $pages->page_4->ac_input_phase or '' }} Phase</td>
                            <td>{{ $pages->page_4->ac_input_frequency or '' }} Hz</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Output Voltage</td>
                            <td>{{ $pages->page_4->dc_output_voltage_low or '' }} VDC</td>
                            <td>to</td>
                            <td>{{ $pages->page_4->dc_output_voltage_high or '' }} VDC</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Float Voltage</td>
                            <td>{{ $pages->page_4->float_voltage or '' }} VDC</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Equalize Voltage</td>
                            <td>{{ $pages->page_4->equalize_voltage or '' }} VDC</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Polarity</td>
                            @if ($pages->page_4->polarity == 'POSITIVE')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> Positive</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> Negative</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> Positive</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> Negative</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Load Test</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Load Current</td>
                            <td>{{ $pages->page_4->load_test_load_current or '' }} A</td>
                            <td>Output Voltage</td>
                            <td>{{ $pages->page_4->load_test_output_voltage or '' }} VDC</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Alarm Test</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.a</td>
                            <td>Low Float</td>
                            @if ($pages->page_4->alarm_test->low_float == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.b</td>
                            <td>Low Load</td>
                            @if ($pages->page_4->alarm_test->low_load == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.c</td>
                            <td>High Float</td>
                            @if ($pages->page_4->alarm_test->high_float == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.d</td>
                            <td>High Load</td>
                            @if ($pages->page_4->alarm_test->high_load == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.e</td>
                            <td>Load Fuse Fail</td>
                            @if ($pages->page_4->alarm_test->load_fuse_fail == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>11.f</td>
                            <td>Battery Fuse Fail</td>
                            @if ($pages->page_4->alarm_test->battery_fuse_fail == 'Pass')
                            <td><img src="img/checkbox-checked.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-default.png" class="checkbox"> NOK</td>
                            @else
                            <td><img src="img/checkbox-default.png" class="checkbox"> OK</td>
                            <td><img src="img/checkbox-checked.png" class="checkbox"> NOK</td>
                            @endif
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>Rectifier System Voltage</td>
                            <td>{{ $pages->page_4->rectifier_system_voltage or '' }} VDC</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </li>

            <div class="page-break"></div>


            <li><h4>B. Battery</h4>
                <table>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Battery type</td>
                            <td>{{ $pages->page_5->battery_type or '' }}</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Capacity</td>
                            <td>{{ $pages->page_5->battery_capacity or '' }} AH</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Voltage per block</td>
                            <td>{{ $pages->page_5->voltage_per_block or '' }} V</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>No of bank</td>
                            <td>{{ $pages->page_5->no_of_bank or '' }} bank</td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>Charging rate</td>
                            <td>{{ $pages->page_5->charging_rate or '' }} of C</td>
                        </tr>
                        <tr>
                            <td>6.</td>
                            <td>Discharge rate</td>
                            <td>(table 1)</td>
                        </tr>
                    </tbody>
                </table>

                <h5>Battery Voltage Discharge Test Table</h5>
                <table class="table table-bordered table-center-heading">
                    <tbody>
                        <tr>
                            <th rowspan="2">Time</th>
                            <th colspan="5">Battery Bank 1 (Load current: {{ $pages->page_5->discharge->bank_1->load_current or 'N/A' }} A)</th>
                            <th colspan="5">Battery Bank 2 (Load current: {{ $pages->page_5->discharge->bank_2->load_current or 'N/A' }} A)</th>
                        </tr>
                        <tr>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>Total</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>Total</th>
                        </tr>
                        <tr>
                            <td class="align-right">0</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_0->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_0->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_0->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_0->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_0->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_0->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_0->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_0->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_0->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_0->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">30</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_30->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_30->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_30->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_30->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_30->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_30->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_30->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_30->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_30->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_30->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">60</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_60->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_60->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_60->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_60->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_60->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_60->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_60->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_60->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_60->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_60->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">90</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_90->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_90->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_90->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_90->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_90->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_90->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_90->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_90->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_90->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_90->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">120</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_120->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_120->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_120->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_120->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_120->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_120->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_120->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_120->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_120->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_120->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">150</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_150->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_150->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_150->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_150->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_150->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_150->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_150->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_150->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_150->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_150->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">180</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_180->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_180->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_180->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_180->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_180->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_180->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_180->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_180->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_180->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_180->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">210</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_210->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_210->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_210->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_210->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_210->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_210->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_210->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_210->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_210->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_210->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">240</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_240->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_240->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_240->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_240->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_240->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_240->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_240->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_240->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_240->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_240->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">270</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_270->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_270->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_270->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_270->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_270->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_270->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_270->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_270->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_270->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_270->total or '' }}</td>
                        </tr>
                        <tr>
                            <td class="align-right">300</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_300->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_300->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_300->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_300->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_1->t_300->total or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_300->b_1 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_300->b_2 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_300->b_3 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_300->b_4 or '' }}</td>
                            <td>{{ $pages->page_5->discharge->bank_2->t_300->total or '' }}</td>
                        </tr>
                    </tbody>
                </table>
            </li>
        </ol>


        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Battery #</th>
                    <th>S/N</th>
                    <th>Voltage after 30 minutes</th>
                </tr>
                <tr>
                    <td>Bank #1<br>Battery #1</td>
                    <td><img src="{{ $pages->bank_1_battery_1_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_1_battery_1_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #1<br>Battery #2</td>
                    <td><img src="{{ $pages->bank_1_battery_2_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_1_battery_2_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #1<br>Battery #3</td>
                    <td><img src="{{ $pages->bank_1_battery_3_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_1_battery_3_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #1<br>Battery #4</td>
                    <td><img src="{{ $pages->bank_1_battery_4_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_1_battery_4_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>

        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Battery #</th>
                    <th>S/N</th>
                    <th>Voltage after 30 minutes</th>
                </tr>
                <tr>
                    <td>Bank #2<br>Battery #1</td>
                    <td><img src="{{ $pages->bank_2_battery_1_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_2_battery_1_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #2<br>Battery #2</td>
                    <td><img src="{{ $pages->bank_2_battery_2_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_2_battery_2_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #2<br>Battery #3</td>
                    <td><img src="{{ $pages->bank_2_battery_3_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_2_battery_3_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #2<br>Battery #4</td>
                    <td><img src="{{ $pages->bank_2_battery_4_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_2_battery_4_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>


        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Battery #</th>
                    <th>S/N</th>
                    <th>Voltage after 30 minutes</th>
                </tr>
                <tr>
                    <td>Bank #3<br>Battery #1</td>
                    <td><img src="{{ $pages->bank_3_battery_1_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_3_battery_1_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #3<br>Battery #2</td>
                    <td><img src="{{ $pages->bank_3_battery_2_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_3_battery_2_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #3<br>Battery #3</td>
                    <td><img src="{{ $pages->bank_3_battery_3_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_3_battery_3_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #3<br>Battery #4</td>
                    <td><img src="{{ $pages->bank_3_battery_4_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_3_battery_4_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>


        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Battery #</th>
                    <th>S/N</th>
                    <th>Voltage after 30 minutes</th>
                </tr>
                <tr>
                    <td>Bank #4<br>Battery #1</td>
                    <td><img src="{{ $pages->bank_4_battery_1_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_4_battery_1_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #4<br>Battery #2</td>
                    <td><img src="{{ $pages->bank_4_battery_2_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_4_battery_2_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #4<br>Battery #3</td>
                    <td><img src="{{ $pages->bank_4_battery_3_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_4_battery_3_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Bank #4<br>Battery #4</td>
                    <td><img src="{{ $pages->bank_4_battery_4_sn or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td><img src="{{ $pages->bank_4_battery_4_voltage_30 or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>



        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Item</th>
                    <th>Photo</th>
                </tr>
                <tr>
                    <td>Site ID/ Location</td>
                    <td><img src="{{ $pages->site_id_location or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Site All</td>
                    <td><img src="{{ $pages->site_all or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>KWH Panel</td>
                    <td><img src="{{ $pages->kwh_panel or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>ACPDB / COS</td>
                    <td><img src="{{ $pages->acpdb_cos or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>



        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Item</th>
                    <th>Photo</th>
                </tr>
                <tr>
                    <td>Rectifier Open</td>
                    <td><img src="{{ $pages->rectifier_open or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Rectifier Closed</td>
                    <td><img src="{{ $pages->rectifier_closed or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>AC Input Rectifier</td>
                    <td><img src="{{ $pages->ac_input_rectifier or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>MCB Output Rectifier</td>
                    <td><img src="{{ $pages->mcb_output_rectifier or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>


        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Item</th>
                    <th>Photo</th>
                </tr>
                <tr>
                    <td>Rectifier Module</td>
                    <td><img src="{{ $pages->rectifier_module_photo or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Battery</td>
                    <td><img src="{{ $pages->battery or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Alarm Relay</td>
                    <td><img src="{{ $pages->alarm_relay or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td>Rectifier Alarm</td>
                    <td><img src="{{ $pages->rectifier_alarm or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
            </tbody>
        </table>



        <div class="page-break"></div>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>Item</th>
                    <th colspan="2">Photo</th>
                </tr>
                <tr>
                    <td>Rectifier Display</td>
                    <td>
                        <img src="{{ $pages->rectifier_display_ac or 'img/no_image.jpg' }}" class="doc-image"><br>
                        On AC Power
                    </td>
                    <td>
                        <img src="{{ $pages->rectifier_display_backup or 'img/no_image.jpg' }}" class="doc-image"><br>
                        On Backup Power
                    </td>
                </tr>
                <tr>
                    <td>Rectifier Display</td>
                    <td>
                        <img src="{{ $pages->rectifier_display_active_alarm or 'img/no_image.jpg' }}" class="doc-image"><br>
                        Active Alarm
                    </td>
                    <td>
                        <img src="{{ $pages->system_value or 'img/no_image.jpg' }}" class="doc-image"><br>
                        System Value
                    </td>
                </tr>
                <tr>
                    <td>Installation</td>
                    <td>
                        <img src="{{ $pages->power_cable_installation or 'img/no_image.jpg' }}" class="doc-image"><br>
                        Power Cable Installation
                    </td>
                    <td>
                        <img src="{{ $pages->grounding_installation or 'img/no_image.jpg' }}" class="doc-image"><br>
                        Grounding Installation
                    </td>
                </tr>
            </tbody>
        </table>

    </div>






</body>
</html>

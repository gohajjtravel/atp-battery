<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.9
    </div>
    <strong>Copyright &copy; 2017 <a href="https://gohajj.id">Tokappa</a>.</strong>
</footer>

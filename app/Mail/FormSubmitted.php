<?php

namespace App\Mail;


use App\FormData;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class FormSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $form_data;

    public $tries = 1;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FormData $form_data)
    {
        // $parameters = json_decode($this->form_data->parameters, true);
        $this->form_data = $form_data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $parameters = json_decode($this->form_data->parameters, true);
        // $site_name  = (empty($parameters['SITE_NAME']) ? 'NO_NAME' : $parameters['SITE_NAME']);
        $form_data = $this->form_data;
        return $this->subject('Form Submitted — Site '.$form_data->site_name)
                    ->view('emails.form_submitted')
                    ->with([
                        'site_name' => $form_data->site_name,
                    ])
                    // ->attach($this->form_data->report_path, [
                    //         'as' => 'report.pdf',
                    //         'mime' => 'application/pdf',
                    //     ])
                    ;
    }
}

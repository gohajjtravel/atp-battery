<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('page-title') - {{ config('app.name', 'Laravel') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendors/ionicons/css/ionicons.min.css') }}">
    <!-- IconMonstr -->
    <link rel="stylesheet" href="{{ asset('vendors/iconmonstr/css/iconmonstr-iconic-font.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/skins/skin-purple-light.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('vendors/icheck/square/blue.css') }}">

    @yield('page-style')

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">


    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="shortcut icon"  href="{{ asset('img/favicon.png') }}">
    <link rel="icon" sizes="130x128" href="{{ asset('img/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="130x128" href="{{ asset('img/favicon.png') }}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple-light @yield('body-class')">

    @yield('page-content')


    @yield('modals')
    <!-- jQuery 3 -->
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('vendors/icheck/icheck.min.js') }}"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(function () {
        'use strict'

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            // increaseArea: '20%' // optional
        });

        $('[data-toggle="push-menu"]').pushMenu();
        var $pushMenu       = $('[data-toggle="push-menu"]').data('lte.pushmenu')

        function changeLayout(cls) {
            $('body').toggleClass(cls)
            $layout.fixSidebar()
            if ($('body').hasClass('fixed') && cls == 'fixed') {
                $pushMenu.expandOnHover()
                $layout.activate()
            }
            $controlSidebar.fix()
        }

        $('[data-enable="expandOnHover"]').on('click', function () {
            $(this).attr('disabled', true)
            $pushMenu.expandOnHover()
            if (!$('body').hasClass('sidebar-collapse'))
            $('[data-layout="sidebar-collapse"]').click()
        })

        $(".alert-dismissible").fadeTo(3000, 300).slideUp(1000);

    });
    </script>

    @yield('javascripts')
</body>
</html>

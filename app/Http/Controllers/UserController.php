<?php

namespace App\Http\Controllers;


use App\Role;
use App\User;
use App\Surveyor;


use Jenssegers\Optimus\Optimus;
use Yajra\Datatables\Datatables;


use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UserController extends Controller
{

    public function __construct(Request $request, Optimus $optimus)
    {
        $this->middleware('auth');
        $this->request  = $request;
        $this->optimus  = $optimus;
    }



    public function index()
    {
        // $users = User::all();
        return view('pages.user-index');
        // return $users;
    }


    public function getData()
    {
        return Datatables::of(User::get())
                    ->whitelist(['name', 'username', 'email', 'role_display_name'])
                    ->addColumn('actions', 'layouts.datatables.actions')
                    ->rawColumns(['actions'])
                    ->make(true);
    }



    public function show()
    {
        // Get user detail from specified id
        $id         = $this->request->input('id');
        $user       = User::findOrFail($this->optimus->decode($id));
        return $user;
    }



    public function store()
    {

        // Assign request
        $request        = $this->request;

        // Validate request
        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6|confirmed',
            'role'      => [
                'required',
                Rule::in(['administrator', 'staff']),
            ],
        ]);

        // Create new user
        $user           = new User;
        $user->name     = $request->input('name');
        $user->email    = strtolower(trim($request->input('email')));
        $user->password = bcrypt($request->input('password'));
        if ( $request->has('dashboard_access') )
        {
            $user->is_active = 1;
        }
        $user->save();

        // Apply role for newly created user
        $role           = Role::where('name', $request->input('role'))->first();
        $user->attachRole($role);

        // Send the good news
        return back()->with('success', __('messages.user_created_successfully'));

    }



    public function update()
    {
        $request        = $this->request;

        // Check user if valid
        $user           = User::findOrFail($this->optimus->decode($request->input('id')));

        // Validate request
        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'password'  => 'sometimes|confirmed',
            'role'      => [
                'required',
                Rule::in(['administrator', 'staff']),
            ],
        ]);

        $user->name     = $request->input('name');
        $user->email    = $request->input('email');

        if ($request->has('password'))
        {
            $user->password = bcrypt($request->input('password'));
        }

        if ( ( $request->has('dashboard_access') ) AND ( ! empty($request->input('dashboard_access')) ) )
        {
            $user->is_active = 1;
        }
        else
        {
            $user->is_active = 0;
        }
        $user->save();

        // Detach from existing role
        $user->roles()->detach();

        // Apply new role
        $role           = Role::where('name', $request->input('role'))->first();
        $user->attachRole($role);

        // Send the good news
        return back()->with('success', __('messages.user_updated_successfully'));
    }



    public function destroy()
    {

        // Assign request
        $request        = $this->request;

        // Check user if valid
        $user           = User::findOrFail($this->optimus->decode($request->input('id')));

        // Do the actual deletion
        $user->delete();

        // Send the good news
        return back()->with('success', __('messages.user_deleted_successfully'));

    }
}

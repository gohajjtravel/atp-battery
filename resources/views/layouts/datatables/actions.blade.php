<button type="button" class="btn btn-primary btn-xs edit-data-button" data-id="{{ $hashed_id }}" data-toggle="modal" data-target="#editModal">Edit</button>
<button type="button" class="btn btn-danger btn-xs delete-data-button" data-id="{{ $hashed_id }}" data-toggle="modal" data-target="#deleteModal">Delete</button>

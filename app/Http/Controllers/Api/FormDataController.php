<?php

namespace App\Http\Controllers\Api;


use App\Role;
use App\FormData;


use Uuid;
use Image;
use JWTAuth;
use Carbon\Carbon;
use Jenssegers\Optimus\Optimus;


use App\Mail\FormSubmitted;
use App\Jobs\ReportGenerator;


use Log;
use Mail;
use Illuminate\Http\Request;
use InvalidArgumentException;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class FormDataController extends Controller
{


    public function __construct(Request $request, Optimus $optimus)
    {
        $this->middleware('jwt.auth', ['except' => ['reporter', 'del_temp']]);
        // $this->middleware('jwt.refresh');
        $this->request  = $request;
        $this->optimus  = $optimus;
    }



    // Store answers
    public function store()
    {
        // Store answers, text only
        // For images, return form_id

        // TODO: I think we need to add validation: site_name not null

        $request        = $this->request;
        $user           = JWTAuth::parseToken()->authenticate();

        Log::info('Form submission initiation:', $request->all());

        try
        {
            $now        = Carbon::now();
            $form_data  = new FormData;

            $form_data->user_id     = $user->id;
            $form_data->site_name   = $request->has('data.SITE_NAME') ? $request->input('data.SITE_NAME') : "NO_NAME";

            $report_data            = $request->input('data');

            // Logic to get current date
            $current_date                   = new Carbon;
            setlocale(LC_TIME, 'id_ID.utf8');
            $report_data['ACCEPTANCE_DATE'] = $current_date->formatLocalized('%d %B %Y');
            $report_data['HARI_ATP']        = $current_date->formatLocalized('%A');
            $report_data['TANGGAL_ATP']     = $current_date->formatLocalized('%d');
            $report_data['BULAN_ATP']       = $current_date->formatLocalized('%B');
            $report_data['TAHUN_ATP']       = $current_date->formatLocalized('%Y');


            // Logic to relate each battery type
            switch ($report_data['BATTERY_TYPE'])
            {
                // New battery type: NDT100
                case "NDT_100":
                    $report_data['BATTERY_BRAND']       = 'NARADA';
                    $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                    $report_data['BATTERY_CAPACITY']    = 100;
                    $report_data['MATERIAL']            = [
                        [
                            'SAP_MATERIAL_NUMBER'       => '2512922',
                            'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,100AH,NARADA,12ND> NARADA 12NDT100S',
                            'MATERIAL_BRAND'            => 'NARADA/NDT',
                            'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                            'RESULT'                    => 1,
                        ],
                        [
                            'SAP_MATERIAL_NUMBER'       => '3413082',
                            'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12',
                            'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                            'MATERIAL_QTY'              => 1,
                            'RESULT'                    => 1,
                        ]
                    ];

                    break;

                // Allow old apps version
                case "ICS_150":
                case "ICS":
                    $report_data['BATTERY_BRAND']       = 'NARADA';
                    $report_data['BATTERY_TYPE_DISPLAY'] = 'ICS';
                    $report_data['BATTERY_CAPACITY']    = 150;
                    $report_data['MATERIAL']            = [
                        [
                            'SAP_MATERIAL_NUMBER'       => '2512280',
                            'MATERIAL_DESCRIPTION'      => 'BATTERY:FAST CHARGING,12VDC,150AH,NARAD>',
                            'MATERIAL_BRAND'            => 'NARADA/ICS',
                            'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                            'RESULT'                    => 1,
                        ],
                        [
                            'SAP_MATERIAL_NUMBER'       => '3413084',
                            'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY FAST CHARGE,12V>',
                            'MATERIAL_BRAND'            => 'RAPTEK/ICS',
                            'MATERIAL_QTY'              => 1,
                            'RESULT'                    => 1,
                        ]
                    ];

                    break;

                // Allow old apps version
                case "NDT_150":
                case "NDT":
                default:
                    $report_data['BATTERY_BRAND']       = 'NARADA';
                    $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                    $report_data['BATTERY_CAPACITY']    = 150;
                    $report_data['MATERIAL']            = [
                        [
                            'SAP_MATERIAL_NUMBER'       => '2512279',
                            'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,150AH,NARADA,NDT1>',
                            'MATERIAL_BRAND'            => 'NARADA/NDT',
                            'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                            'RESULT'                    => 1,
                        ],
                        [
                            'SAP_MATERIAL_NUMBER'       => '3413083',
                            'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12V150AH',
                            'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                            'MATERIAL_QTY'              => 1,
                            'RESULT'                    => 1,
                        ]
                    ];

            }

            // Populate BATTERY_SN
            $battery_sns = [];
            foreach ($report_data['NEW_BATTERY'] as $new_battery)
            {
                if ( ! empty($new_battery['SN']) )
                {
                    $battery_sns[] = $new_battery['SN'];
                }
            }

            $report_data['BATTERY_SN'] = implode($battery_sns, ', ');


            $form_data->parameters  = json_encode($report_data);
            $form_data->save();

            Log::info('Form data saved:', $form_data->toArray());
            return response()->json([
                'success' => TRUE,
                'form_id' => $form_data->id,
                'message' => 'form_data_saved'
            ]);

        }

        catch (Exception $e)
        {
            Log::info('Unknown error.', $request->all);
            return response()->json(['success' => FALSE, 'message' => 'unknown_error']);
        }
    }


    // Handle image upload
    // public function update($form_data_id, $form_field)
    public function update($form_data_id)
    {
        // Parameters:
        // - form_id
        // - form_field -> UPDATE: we only receive zip file now

        $request    = $this->request;

        // Check if we have a file request
        if ( ! $request->hasFile('images'))
        {
            Log::info('Empty images: ', ["id" => $form_data_id]);
            return response()->json(['success' => FALSE, 'message' => 'empty_images']);
        }

        Log::info('Processing images initiation: ', ["id" => $form_data_id]);
        $form_data      = FormData::findOrFail($form_data_id);

        // Workflow:
        // 1. Store and rename zip file
        $zip_uuid       = Uuid::generate(4);
        $zip_path       = $request->file('images')->storeAs(
            'uploads', $zip_uuid.'.zip', 'public'
        );
        $absolute_zip_path = storage_path('app/public/'.$zip_path);


        // 2. Uncompress zip file
        $zip_file       = new \ZipArchive;
        $res            = $zip_file->open($absolute_zip_path);
        if ($res !== TRUE)
        {
            Log::info('Zip file invalid: ', ["id" => $form_data_id]);
            return response()->json(['success' => FALSE, 'message' => 'invalid_zip']);
        }
        $extraction_path    = '/tmp/'.$zip_uuid;
        $zip_file->extractTo($extraction_path);
        $zip_file->close();

        // 3. Read mapping file
        $mapping_file   = $extraction_path.'/mapping.json';
        $handle         = fopen($mapping_file, "r");
        $contents       = fread($handle, filesize($mapping_file));
        fclose($handle);

        $decoded_mapping = json_decode($contents);

        // 4. Process the images
        $parameters         = json_decode($form_data->parameters, true);
        foreach ($decoded_mapping as $report_image)
        {
            $image_filename = Uuid::generate(4).'.jpg';
            $image_path     = 'storage/uploads/'. $image_filename;
            $img            = Image::make($extraction_path.'/'.$report_image->filename);
            $img->save($image_path);

            // $report_images[$report_image->name] = $image_path;
            $param_name     = $report_image->name;
            $parameters['IMAGES'][$param_name]  = $image_path;
        }
        $form_data->parameters      = json_encode($parameters);
        $form_data->save();

        // TODO:
        // 5. Cleanup temporary files
        $this->deleteDirectory($extraction_path);

        // Logging
        Log::info('Update images successful: ', ["id" => $form_data_id]);

        return response()->json(['success' => TRUE, 'message' => 'images_saved_successfully']);


    }


    // Report generation dispatcher
    // 1. Notify admin via email (blank images)
    // 2. Notify user via email (with images)
    public function reporter($form_data_id)
    {
        $request    = $this->request;
        // $staff      = JWTAuth::parseToken()->authenticate();
        // $staff      = \App\User::find(2);

        try
        {

            $form_data  = FormData::with('user')->findOrFail($form_data_id);
            $parameters = json_decode($form_data->parameters, true);

            $staff      = $form_data->user;

            // Dispatch PDF Generator
            // $job = (new ReportGenerator($form_data))->delay(Carbon::now());
            $job = (new ReportGenerator($form_data))->delay(Carbon::now()->addSeconds(45));
            dispatch($job);

            // Notify admin
            $admin_role     = Role::where('name', 'administrator')->first();
            $admin_users    = $admin_role->users;

            $carbon_copies  = ['rpt@gmail.com'];
            if (filter_var($parameters['EMAIL'], FILTER_VALIDATE_EMAIL))
            {
                $carbon_copies[] = $parameters['EMAIL'];
            }

            // Send notification
            $when           = Carbon::now()->addSeconds(90);
            // $when           = Carbon::now();
            Mail::to($staff)
                ->cc($carbon_copies)
                ->bcc($admin_users)
                ->later($when, new FormSubmitted($form_data));

            Log::info('PDF report dispatched: ', ["id" => $form_data_id]);
            return response()->json(['success' => TRUE, 'message' => 'pdf_report_dispatched']);


        }
        catch (ModelNotFoundException $e)
        {
            return response()->json(['success' => FALSE, 'message' => 'assignment_or_question_not_found']);
        }
        catch (Exception $e)
        {
            return response()->json(['success' => FALSE, 'message' => 'unknown_error']);
        }
    }



    /**
    * Recursively deletes a directory tree.
    *
    * @param string $folder         The directory path.
    * @param bool   $keepRootFolder Whether to keep the top-level folder.
    *
    * @return bool TRUE on success, otherwise FALSE.
    */
    private function deleteDirectory(
        $folder,
        $keepRootFolder = false)
        {
            // Handle bad arguments.
            if (empty($folder) || !file_exists($folder)) {
                return true; // No such file/folder exists.
            } elseif (is_file($folder) || is_link($folder)) {
                return @unlink($folder); // Delete file/link.
            }

            // Delete all children.
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($folder, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::CHILD_FIRST
            );

            foreach ($files as $fileinfo) {
                $action = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
                if (!@$action($fileinfo->getRealPath())) {
                    return false; // Abort due to the failure.
                }
            }

            // Delete the root folder itself?
            return (!$keepRootFolder ? @rmdir($folder) : true);
        }

}

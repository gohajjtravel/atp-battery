<?php

namespace App\Http\Controllers\Api;


use JWTAuth;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();
        // $surveyor = $user->surveyor;
        return response()->json($user);
    }
}

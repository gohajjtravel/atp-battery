<?php

return [
    'header' => [
        'profile' => 'Profile',
        'signout' => 'Signout',
        'language' => 'Choose Language',
        'english' => 'English',
        'indonesia' => 'Indonesia',
    ],
    'sidebar' => [
        'search-menu' => 'Search Menu',
        'users' => 'Users',
        'form-data' => 'Form Data',
        'main-navigation' => 'Navigation',
        'report' => 'Report'
    ],
];

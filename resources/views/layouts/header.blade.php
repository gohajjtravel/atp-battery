<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('index') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><img src="{{ asset('img/logo-only.png') }}" width="30"></span> -->
        <span class="logo-mini"><strong>A</strong>B</span>
        <!-- logo for regular state and mobile devices -->
        <!-- <span class="logo-lg"><img src="{{ asset('img/logo.png') }}" width="100"></span> -->
        <span class="logo-lg"><strong>ATP</strong> Battery</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">


                <!--
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">@lang('layouts.header.language')</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="{{ route('home.language', 'en') }}">
                                        @lang('layouts.header.english')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('home.language', 'id') }}">
                                        @lang('layouts.header.indonesia')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                -->

                <!-- User Account -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if (Auth::check())
                            @if (Auth::user()->avatar_url == "")
                            <img src="{{ asset('img/default-avatar.jpg') }}" class="user-image" alt="User Image">
                            @else
                            <img src="{{ asset(Auth::user()->avatar_url) }}" class="user-image" alt="User Image">
                            @endif
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if (Auth::check())
                                @if (Auth::user()->avatar_url == "")
                                <img src="{{ asset('img/default-avatar.jpg') }}" class="img-circle" alt="User Image">
                                @else
                                <img src="{{ asset(Auth::user()->avatar_url) }}" class="img-circle" alt="User Image">
                                @endif

                                <p>
                                    {{ Auth::user()->email }}
                                    <small class="user-role">{{ Auth::user()->role_display_name }}</small>
                                    <!-- <small>Member since Nov. 2012</small> -->
                                </p>
                            @endif
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">@lang('layouts.header.profile')</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}"
                                    class="btn btn-default btn-flat"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    @lang('layouts.header.signout')
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>


                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>

    </nav>
</header>

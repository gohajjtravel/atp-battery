<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
})->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{language}', 'HomeController@indexLanguage')->name('home.language');


// Users
Route::get('/user', 'UserController@index')->name('user.index');
Route::get('/user/data', 'UserController@getData')->name('user.data');
Route::get('/user/show', 'UserController@show')->name('user.show');
Route::post('/user', 'UserController@store')->name('user.add');
Route::post('/user/update', 'UserController@update')->name('user.update');
Route::post('/user/destroy', 'UserController@destroy')->name('user.destroy');


// Form data
Route::get('/form-data', 'FormDataController@index')->name('form-data.index');
Route::get('/form-data/data', 'FormDataController@getData')->name('form-data.data');
Route::post('/form-data/approve', 'FormDataController@approveData')->name('form-data.approve');
Route::get('/form-data/show', 'FormDataController@show')->name('form-data.show');
Route::post('/form-data/destroy', 'FormDataController@destroy')->name('form-data.destroy');

Route::get('/form-data-test/{id}', 'FormDataController@test_report');
Route::get('/form-data-gen/{id}', 'FormDataController@test_generate');


// Reports
Route::get('/report', 'ReportController@index')->name('report.index');
Route::get('/report/data', 'ReportController@getData')->name('report.data');
Route::get('/report/excel', 'ReportController@exportExcel')->name('report.excel');
Route::get('/report/{id}', 'ReportController@show')->name('report.show');
Route::post('/form-data', 'FormDataController@store')->name('form-data.store');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('authenticate', 'Auth\JwtLoginController@authenticate');
    // Get user detail
    Route::get('user', 'Api\UserController@index');

    // Route::get('assignment', 'Api\AssignmentController@index');
    // Route::get('assignment/{hashed_assignment_id}', 'Api\AssignmentController@show');
    // Route::get('question', 'Api\QuestionController@index');
    // Route::get('answer', 'Api\AnswerController@index');

    // Send form
    Route::post('form-data', 'Api\FormDataController@store');
    // Route::post('form-data/{form_data_id}/{form_field}', 'Api\FormDataController@update');
    Route::post('form-data/{form_data_id}', 'Api\FormDataController@update');
    Route::get('form-data/report/{form_data_id}', 'Api\FormDataController@reporter');
    // Route::get('form-data/del_temp', 'Api\FormDataController@del_temp');

    Route::post('refresh-token', 'Auth\JwtLoginController@refreshToken');


});

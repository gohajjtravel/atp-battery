<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <style>
    @media print {
        @page { size: A4 portrait; margin: 0; }
        body, .wrap { padding: 0; background: none; border: none; margin: 0; }
        .document { width: 180mm; height: 260mm; margin: 20mm; line-height: 1; }
        .document div { width: 100%; height: 98%; border: 1px solid red; }
    }

    /*
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: normal;
        src: local('Source Sans Pro'), local('SourceSansPro-Regular'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/ODelI1aHBYDBqgeIAH2zlNzbP97U9sKh0jjxbPbfOKg.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: bold;
        src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/toadOcfmlt9b38dHJxOBGLsbIrGiHa6JIepkyt5c0A0.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: italic;
        font-weight: normal;
        src: local('Source Sans Pro Italic'), local('SourceSansPro-It'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/M2Jd71oPJhLKp0zdtTvoM0DauxaEVho0aInXGvhmB4k.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Source Sans Pro';
        font-style: italic;
        font-weight: bold;
        src: local('Source Sans Pro Bold Italic'), local('SourceSansPro-BoldIt'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/fpTVHK8qsXbIeTHTrnQH6Edtd7Dq2ZflsctMEexj2lw.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Arial Narrow';
        font-style: normal;
        font-weight: normal;
        src: url(pdf/fonts/ArialNarrowRegular.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Arial Narrow';
        font-style: normal;
        font-weight: bold;
        src: url(pdf/fonts/ArialNarrowBold.ttf) format('truetype');
    }
    */

    @page {
        margin: 15px 30px;
    }


    body {
        font-family: 'Source Sans Pro';
        font-size: 0.8em;
        line-height: 0.9em;
    }

    p {
        line-height: 0.6em;
    }

    .checkbox {
        width: 10px;
        height: 10px;
        padding: 0 4px;
    }

    ol {
        line-height: 1.2em;
    }

    .table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        page-break-inside: auto;
    }

    .table td {
        padding: 3px;
        line-height: 0.8em;
        vertical-align: top;
    }

    .table th {
        line-height: 1.1em;
    }

    .table-center-heading th {
        text-align: center;
    }

    .table-bordered {
        border: 1px solid #333;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #333;
    }

    .table-relaxed td, .table-relaxed th {
        /* line-height: 1.5em; */
        padding: 0.8em;
    }

    .header,
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
        font-size: 0.8em;
    }
    .footer .pagenum:before {
          content: counter(page);
    }

    .footer .pagecount:before {
          content: counter(pages);
    }

    .header {
        top: 0px;
    }
    .footer {
        bottom: 70px;
    }

    .page-content {
        width: 100%;
        position: absolute;
        top: 90px;
        bottom: 80px;
        /* bottom: 150px; */
    }

        /*p.header {
        padding-top: 10px;
        padding-bottom: -14px;
        font-size: 1em;
        font-weight: bold;
    }

    table .narrow-font {
        padding: 2px;
    }

    .narrow-font {
        font-family: 'Arial Narrow';
        font-size: 1em;
    }


    table > thead> th {
        font-family: "Arial Narrow";
    }

    table.treatment-list > tbody > tr > td:first-child {
        text-align: center;
    }*/

    h1,
    h2,
    h3,
    h4,
    h5,
    .text-center {
        text-align: center;
        /* font-family: "Arial Narrow"; */
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        margin: 10px 0;
    }

    .checkbox {
        width: 10px;
        height: 10px;
        padding: 0 4px;
    }

    .checkbox-rectangle {
        height: 15px;
        padding: 0 4px;
    }

    .checkbox-inline {
        float:right;
        /*margin-left: 20px;*/
    }

    .table-header {
        padding-top: 10px;
        padding-bottom: 10px;
        /* border: 1px dashed #333; */
    }

    .field-header {
        background-color: #eee;
        width: 30%;
    }

    .field-content span {
        padding-right: 10px;
    }

    .logo-header {
        width: 80px;
    }

    .doc-image {
        max-width: 170px;
        max-height: 170px;
    }

    .align-left {
        text-align: left;
    }

    .align-right {
        text-align: right;
    }

    .field-center {
        text-align: center;
    }

    .photo-grid-2 {
        max-width: 260px;
    }

    .photo-grid-4 {
        max-width: 160px;
    }

    table p {
        padding: 0;
        margin: 0;
        line-height: 0.9;
    }

    /*tr.title {
        background-color: #ccc;
    }

    tr.subtitle {
        background-color: #eee;
        font-weight: bold;
    }

    span.emphasis {
        border-bottom: 1px solid #000;
    }*/
    .page-break {
        page-break-after: always;
    }

    </style>



</head>

<body>


    <div class="header">
        <table class="table table-header">
            <tbody>
                <tr>
                    <td><img src="img/logo-only.png" class="logo-header"></td>
                    <td class="align-right"><img src="img/xl_logo.png" class="logo-header"></td>
                </tr>
            </tbody>
        </table>
    </div>



    <div class="page-content">


        <h1>PT XL AXIATA</h1>
        <h1>ACCEPTANCE TEST DOCUMENT</h1>
        <h3>BATTERY REPLACEMENT</h3>
        <table class="table">
            <tbody>
                <tr>
                    <td width="30%">VENDOR/CONTRACTOR</td>
                    <td>:</td>
                    <td width="67%">PT. RAJAWALI PRIMA TEKNIK</td>
                </tr>
                <tr>
                    <td>PROJECT NAME</td>
                    <td>:</td>
                    <td>BATTERY REPLACEMENT</td>
                </tr>
                <tr>
                    <td>SITE NAME</td>
                    <td>:</td>
                    <td>{{ $report_data['SITE_NAME'] or '' }}</td>
                </tr>
                <tr>
                    <td>DATE OF ACCEPTANCE</td>
                    <td>:</td>
                    <td>{{ $report_data['ACCEPTANCE_DATE'] or '' }}</td>
                </tr>
                <tr>
                    <td>ACCEPTANCE STATUS</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <tbody>
                <tr>
                    <td width="30%"></td>
                    <td width="5%">
                        <img src="img/rectangle-checked.png" class="checkbox-rectangle"> &nbsp;
                    </td>
                    <td width="45%">PASS, NO PENDING ITEMS</td>
                    <td width="15%"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <img src="img/rectangle-default.png" class="checkbox-rectangle"> &nbsp;
                    </td>
                    <td>PASS WITH PENDING ITEMS</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <img src="img/rectangle-default.png" class="checkbox-rectangle"> &nbsp;
                    </td>
                    <td>REJECTED&nbsp; BY XL AXIATA</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <p>ACCEPTANCE BY:</p>
        <p>PT. RAJAWALI PRIMA TEKNIK</p>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td colspan="3">Person Name &amp; Sign, Date</td>
                </tr>
                <tr>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>XL MANAGE SERVICE PARTNER</p>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td colspan="3">1. FOC Engineer Name &amp; Signature, Date</td>
                </tr>
                <tr>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">2. FOP Engineer Name &amp; Signature, Date</td>
                </tr>
                <tr>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">3. FOP Supervisor Name &amp; Signature, Date</td>
                </tr>
                <tr>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>*) Please see complete into at the additional notes section</p>
        <p>FOR REMOTE FOC SUPERVISOR, THIS PAGE WILL BE FAXED TO FOC SUPERVISOR TO BE REVIEWED, CHECKED AND SIGN</p>








        <div class="page-break"></div>



        <h1>BERITA ACARA UJI TERIMA</h1>
        <p>Pada hari <b>{{ $report_data['HARI_ATP'] or '' }}</b> tanggal <b>{{ $report_data['TANGGAL_ATP'] or '' }}</b>, bulan <b>{{ $report_data['BULAN_ATP'] or '' }}</b>, tahun <b>{{ $report_data['TAHUN_ATP'] or '' }}</b></p>
        <p>Telah diuji terimakan pekerjaan "Instalasi Replacement Battery" dengan keterangan sebagai berikut:</p>
        <ol style="list-style-type: upper-alpha;">
            <li>Site Identification<br />
                <table class="table table-bordered table-relaxed">
                    <tbody>
                        <tr>
                            <th width="30%">Vendor</th>
                            <td>PT. RAJAWALI PRIMA TEKNIK</td>
                        </tr>
                        <tr>
                            <th>Site Name/Number</th>
                            <td>{{ $report_data['SITE_NAME'] or '' }} / {{ $report_data['SITE_ID'] or '' }}</td>
                        </tr>
                        <tr>
                            <th>Site Address</th>
                            <td>{{ $report_data['SITE_ADDRESS'] or '' }}</td>
                        </tr>
                        <tr>
                            <th>Area</th>
                            <td>{{ $report_data['SITE_AREA'] or '' }}</td>
                        </tr>
                        <tr>
                            <th>NBWO/EWO Number</th>
                            <td>{{ $report_data['NBWO_EWO_NUMBER'] or '' }}</td>
                        </tr>
                        <tr>
                            <th>PO Number (Line item)</th>
                            <td>{{ $report_data['PO_NUMBER'] or '' }}</td>
                        </tr>
                        <tr>
                            <th>Reservation Number (If any)</th>
                            <td>{{ $report_data['RESERVATION_NUMBER'] or '' }}</td>
                        </tr>
                    </tbody>
                </table>
            </li>
            <li>Material/Service List Labeling<br />
                <table class="table table-bordered table-relaxed table-center-heading">
                    <tbody>
                        <tr>
                            <th rowspan="2">No.</th>
                            <th rowspan="2">SAP Material / Service Number</th>
                            <th rowspan="2">Material / Service Description</th>
                            <th rowspan="2">Brand/Type</th>
                            <th rowspan="2">Qty</th>
                            <th colspan="2">Result</th>
                        </tr>
                        <tr>
                            <th>OK</th>
                            <th>NOK</th>
                        </tr>
                        @if ( ! empty($report_data['MATERIAL']) )
                        @foreach ($report_data['MATERIAL'] as $material)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $material['SAP_MATERIAL_NUMBER'] }}</td>
                            <td>{{ $material['MATERIAL_DESCRIPTION'] }}</td>
                            <td>{{ $material['MATERIAL_BRAND'] }}</td>
                            <td>{{ $material['MATERIAL_QTY'] }}</td>
                            <td>
                                @if ( ! empty($material['RESULT']) )
                                <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;
                                @else
                                <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                                @endif
                            </td>
                            <td>
                                @if ( ! empty($material['RESULT']) )
                                <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                                @else
                                <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </li>
        </ol>



        <div class="page-break"></div>



        <table class="table table-bordered table-relaxed table-center-heading">
            <tbody>
                <tr>
                    <th>&nbsp;</th>
                    <th width="28%">CHECKED BY</th>
                    <th width="28%">APPROVED BY</th>
                    <th width="28%">CONTRACTOR</th>
                </tr>
                <tr>
                    <td>
                        <p>SIGNATURE</p>
                        <p><br />DATE</p>
                        <p>&nbsp;</p>
                        <p>NAME</p>
                    </td>
                    <td>XL MANAGE SERVICE PARTNER</td>
                    <td>PT. XL AXIATA</td>
                    <td>PT. RAJAWALI PRIMA TEKNIK</td>
                </tr>
                <tr>
                    <td>HP NUMBER</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>EMAIL ADDRESS</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>






        <div class="page-break"></div>




        <h1>BERITA ACARA UJI TERIMA LAPANGAN</h1>
        <p>Pada hari <b>{{ $report_data['HARI_ATP'] or '' }}</b> tanggal <b>{{ $report_data['TANGGAL_ATP'] or '' }}</b>, bulan <b>{{ $report_data['BULAN_ATP'] or '' }}</b>, tahun <b>{{ $report_data['TAHUN_ATP'] or '' }}</b></p>
        <p>Telah dilakukan pengujian battery:</p>
        <p>SN:
            {{ $report_data['BATTERY_SN'] or '' }}
        </p>
        <p>dengan hasil baik.</p>
        <table class="table table-relaxed table-center-heading">
            <tbody>
                <tr>
                    <th width="35%">PT. RAJAWALI PRIMA TEKNIK</th>
                    <th>&nbsp;</th>
                    <th width="35%">XL MANAGE SERVICE PARTNER</th>
                </tr>
                <tr>
                    <td>
                        <p>1. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <p>1. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>2. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <p>2. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
            </tbody>
        </table>





        <div class="page-break"></div>




        <h1>ACCEPTANCE TEST BATTERY</h1>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th width="30%">Date</th>
                    <td>{{ $report_data['ACCEPTANCE_DATE'] or ''}}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td>PT. XL AXIATA</td>
                </tr>
                <tr>
                    <th>Site Name</th>
                    <td>{{ $report_data['SITE_NAME'] or '' }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $report_data['SITE_ADDRESS'] or '' }}</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table">
            <tbody>
                <tr>
                    <td width="5%">1.</td>
                    <td width="25%">AC Input</td>
                    <td>{{ $report_data['AC_INPUT'] or '220' }} VAC &mdash; {{ $report_data['INPUT_PHASE'] or '3' }} Phase &mdash; {{ $report_data['INPUT_FREQUENCY'] or '50' }} Hz</td>
                </tr>
                <tr>
                    <td>2. </td>
                    <td>Output Voltage</td>
                    <td>{{ $report_data['OUTPUT_VOLTAGE_LOW'] or '40' }} VDC to {{ $report_data['OUTPUT_VOLTAGE_HIGH'] or '56' }} VDC</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Float Voltage</td>
                    <td>{{ $report_data['FLOAT_VOLTAGE'] or '50' }}VDC</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Equalize Voltage</td>
                    <td>{{ $report_data['EQUALIZE_VOLTAGE'] or '56' }}VDC</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Battery Type</td>
                    <td>{{ $report_data['BATTERY_TYPE_DISPLAY'] or '' }}</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Capacity</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Voltage per Block</td>
                    <td>{{ $report_data['VOLTAGE_PER_BLOCK'] or '12' }}VDC</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>No of Block</td>
                    <td>{{ $report_data['BATTERY_QTY'] or '' }}</td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>Dicharge Test</td>
                    <td>(ATTACHED)</td>
                </tr>
                <tr>
                    <td>10.</td>
                    <td>Load Test</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>- Load Current</td>
                    <td>(ATTACHED)</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>- Output Voltage</td>
                    <td>(ATTACHED)</td>
                </tr>
                <tr>
                    <td>11.</td>
                    <td>Alarm Test</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>- Battery Fuse Fail</td>
                    <td>
                        @if ( ! empty($report_data['ALARM_BATTERY_FUSE_FAIL']) )
                        <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;PASS&nbsp;&nbsp;/&nbsp;
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;FAIL
                        @else
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;PASS&nbsp;&nbsp;/&nbsp;
                        <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;FAIL
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>12.</td>
                    <td>Rectifier System Voltage</td>
                    <td>{{ $report_data['RECTIFIER_SYSTEM_VOLTAGE'] or '48' }}VDC</td>
                </tr>
                <tr>
                    <td>13.</td>
                    <td>NMS/OSS Power</td>
                    <td>
                        @if ( ! empty($report_data['NMM_OSS_POWER']) )
                        <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;Connect&nbsp;&nbsp;/&nbsp;
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;NOT Connect
                        @else
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;Connect&nbsp;&nbsp;/&nbsp;
                        <img src="img/checkbox-checked.png" class="checkbox"> &nbsp;NOT Connect
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>







        <div class="page-break"></div>




        <h3>BATTERY SERIAL NUMBER AND VOLTAGE AFTER 60 MINUTES ATP</h3>
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>SERIAL NUMBER</th>
                    <th>VOLTAGE AFTER 60 MINUTES</th>
                    <th>SERIAL NUMBER</th>
                    <th>VOLTAGE AFTER 60 MINUTES</th>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_1_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_1_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_2_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_2_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 1</b></td>
                    <td colspan="2" class="text-center"><b>Blok 2</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_3_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_3_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_4_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_4_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 3</b></td>
                    <td colspan="2" class="text-center"><b>Blok 4</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_5_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_5_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_6_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_6_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 5</b></td>
                    <td colspan="2" class="text-center"><b>Blok 6</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_7_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_7_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_8_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_8_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 7</b></td>
                    <td colspan="2" class="text-center"><b>Blok 8</b></td>
                </tr>
            </tbody>
        </table>




        <div class="page-break"></div>



        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th>SERIAL NUMBER</th>
                    <th>VOLTAGE AFTER 60 MINUTES</th>
                    <th>SERIAL NUMBER</th>
                    <th>VOLTAGE AFTER 60 MINUTES</th>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_9_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_9_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_10_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_10_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 9</b></td>
                    <td colspan="2" class="text-center"><b>Blok 10</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_11_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_11_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_12_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_12_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 11</b></td>
                    <td colspan="2" class="text-center"><b>Blok 12</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_13_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_13_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_14_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_14_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 13</b></td>
                    <td colspan="2" class="text-center"><b>Blok 14</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_15_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_15_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_16_SN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATT_16_VOLTAGE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Blok 15</b></td>
                    <td colspan="2" class="text-center"><b>Blok 16</b></td>
                </tr>
            </tbody>
        </table>




        <div class="page-break"></div>

        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th width="30%">Date</th>
                    <td>{{ $report_data['ACCEPTANCE_DATE'] or ''}}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td>PT. XL AXIATA</td>
                </tr>
                <tr>
                    <th>Site Name</th>
                    <td>{{ $report_data['SITE_NAME'] or '' }}</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_CLOSED'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_MODULE'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_MCB'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Rectifier Open</b></td>
                    <td class="text-center"><b>Rectifier Closed</b></td>
                    <td class="text-center"><b>Rectifier Module</b></td>
                    <td class="text-center"><b>MCB Battery</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_CABLE_INSTALLATION'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_1_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_1_CLOSED'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_2_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Battery Cable Installation</b></td>
                    <td class="text-center"><b>Battery Rack 1 Open</b></td>
                    <td class="text-center"><b>Battery Rack 1 Closed</b></td>
                    <td class="text-center"><b>Battery Rack 2 Open</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_2_CLOSED'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_3_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_3_CLOSED'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_FUSE_FAIL'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Battery Rack 2 Closed</b></td>
                    <td class="text-center"><b>Battery Rack 3 Open</b></td>
                    <td class="text-center"><b>Battery Rack 3 Closed</b></td>
                    <td class="text-center"><b>Battery Fuse Fail</b></td>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_DISPLAY_AC'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_DISPLAY_BACKUP'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_DISPLAY_AC'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Display Rect On AC Power</b></td>
                    <td class="text-center"><b>Display Rect On Backup Batt</b></td>
                    <td class="text-center"><b>Display Active Alarm</b></td>
                    <td></td>
                </tr>
            </tbody>
        </table>





        <div class="page-break"></div>




        <table class="table table-bordered table-relaxed">
            <tbody>
                <tr>
                    <th>BRAND</th>
                    <td>{{ $report_data['BATTERY_BRAND'] or '' }}</td>
                    <th>LOAD</th>
                    <td>{{ $report_data['LOAD_CURRENT'] or '' }} A</td>
                </tr>
                <tr>
                    <th>TYPE BATTERY</th>
                    <td>{{ $report_data['BATTERY_TYPE_DISPLAY'] or '' }}</td>
                    <th>DISCHARGE TIME</th>
                    <td>{{ $report_data['DISCHARGE_TIME'] or '60'}} mnt</td>
                </tr>
                <tr>
                    <th>QUANTITY</th>
                    <td>{{ $report_data['BATTERY_QTY'] or '' }} Pcs</td>
                    <th rowspan="2">END VOLTAGE</th>
                    <td rowspan="2">
                        ________V/Cell<br>
                        ________V/Block<br>
                        ________V/String
                    </td>
                </tr>
                <tr>
                    <th>TEMPERATURE</th>
                    <td>{{ $report_data['TEMPERATURE'] or '' }} &deg;C</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>

        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th rowspan="2" width="8%">BATT</th>
                    <th rowspan="2">SERIAL NUMBER</th>
                    <th colspan="5">VOLTAGE (V)</th>
                    <th colspan="5">CURRENT (A)</th>
                    <th rowspan="2">CAPACITY</th>
                    <th colspan="2">REMARK</th>
                </tr>
                <tr>
                    <th width="5%">00:00</th>
                    <th width="5%">00:15</th>
                    <th width="5%">00:30</th>
                    <th width="5%">00:45</th>
                    <th width="5%">01:00</th>
                    <th width="5%">00:00</th>
                    <th width="5%">00:15</th>
                    <th width="5%">00:30</th>
                    <th width="5%">00:45</th>
                    <th width="5%">01:00</th>
                    <th>OK</th>
                    <th>NOK</th>
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_1']['SN']) )

                    <td>1</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_1']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_2']['SN']) )

                    <td>2</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_2']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_3']['SN']) )

                    <td>3</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_3']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_4']['SN']) )

                    <td>4</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_4']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_5']['SN']) )

                    <td>5</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_5']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_6']['SN']) )

                    <td>6</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_6']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_7']['SN']) )

                    <td>7</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_7']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_8']['SN']) )

                    <td>8</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_8']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_9']['SN']) )

                    <td>9</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_9']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_10']['SN']) )

                    <td>10</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_10']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_11']['SN']) )

                    <td>11</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_11']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_12']['SN']) )

                    <td>12</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_12']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_13']['SN']) )

                    <td>13</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_13']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_14']['SN']) )

                    <td>14</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_14']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_15']['SN']) )

                    <td>15</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_15']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>
                <tr>
                    @if ( isset($report_data['NEW_BATTERY']['BATT_16']['SN']) )

                    <td>16</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['SN'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['T0'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['T1'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['T2'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['T3'] or ''}}</td>
                    <td>{{ $report_data['NEW_BATTERY']['BATT_16']['T4'] or ''}}</td>

                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['LOAD_CURRENT'] or ''}}</td>
                    <td>{{ $report_data['BATTERY_CAPACITY'] or '150' }}AH</td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>
                    <td>
                        <img src="img/checkbox-default.png" class="checkbox"> &nbsp;
                    </td>

                    @else

                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    @endif
                </tr>


            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table table-bordered table-relaxed table-center-heading">
            <tbody>
                <tr>
                    <th>PT. XL Axiata Tbk.</th>
                    <th>XL Manage Service Partner</th>
                    <th>PT. Rajawali Prima Teknik</th>
                </tr>
                <tr>
                    <td>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Place / Date:</td>
                    <td>Place / Date:</td>
                    <td>Place / Date:</td>
                </tr>
            </tbody>
        </table>












        <div class="page-break"></div>



















        <h1>BERITA ACARA PENARIKAN MATERIAL</h1>
        <table class="table">
            <tbody>
                <tr>
                    <td width="4%">1</td>
                    <td width="30%">DATE</td>
                    <td width="2%">:</td>
                    <td>{{ $report_data['ACCEPTANCE_DATE'] or '' }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>CUSTOMER</td>
                    <td>:</td>
                    <td>PT. XL AXIATA TBK.</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>CONTRACT NUMBER</td>
                    <td>:</td>
                    <td>{{ $report_data['CONTRACT_NUMBER'] or '' }}</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>SITE ID</td>
                    <td>:</td>
                    <td>{{ $report_data['SITE_ID'] or '' }}</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>SITE NAME</td>
                    <td>:</td>
                    <td>{{ $report_data['SITE_NAME'] or '' }}</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>SITE ADDRESS</td>
                    <td>:</td>
                    <td>{{ $report_data['SITE_ADDRESS'] or '' }}</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>PIC</td>
                    <td>:</td>
                    <td>{{ $report_data['INSTALLER_NAME'] or '' }}</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th width="30%">Material</th>
                    <th width="30%">Brand</th>
                    <th>Qty</th>
                </tr>
                <tr>
                    <td>Battery</td>
                    <td>{{ $report_data['OLD_BATTERY']['BRAND'] or '' }}</td>
                    <td>{{ $report_data['OLD_BATTERY']['QTY'] or '' }}</td>
                </tr>
                <tr>
                    <td>Other</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <p>Pada hari <b>{{ $report_data['HARI_ATP'] or '' }}</b> tanggal <b>{{ $report_data['TANGGAL_ATP'] or '' }}</b>, bulan <b>{{ $report_data['BULAN_ATP'] or '' }}</b>, tahun <b>{{ $report_data['TAHUN_ATP'] or '' }}</b></p>
        <p>Telah dilakukan penarikan material ex swap dari site {{ $report_data['SITE_ID'] or '' }} / {{ $report_data['SITE_NAME'] or '' }} dengan hasil {{ $report_data['HASIL_PENARIKAN_MATERIAL'] or 'baik'}}</p>
        <p>&nbsp;</p>
        <table class="table table-relaxed table-center-heading">
            <tbody>
                <tr>
                    <th width="35%">PT. RAJAWALI PRIMA TEKNIK</th>
                    <th>&nbsp;</th>
                    <th width="35%">XL MANAGE SERVICE PARTNER</th>
                </tr>
                <tr>
                    <td>
                        <p>1. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <p>1. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>2. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <p>2. </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;</u></p>
                    </td>
                </tr>
            </tbody>
        </table>







        <div class="page-break"></div>











        <h3>Lampiran Foto</h3>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th width="30%">Date</th>
                    <td>{{ $report_data['ACCEPTANCE_DATE'] or ''}}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td>PT. XL AXIATA</td>
                </tr>
                <tr>
                    <th>Site Name</th>
                    <td>{{ $report_data['SITE_NAME'] or '' }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $report_data['SITE_ADDRESS'] or '' }}</td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <th width="50%">BEFORE</th>
                    <th>BEFORE</th>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['OLD_RECTIFIER'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['OLD_BATTERY'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Rectifier System with Module/Load</b></td>
                    <td class="text-center"><b>Battery Existing</b></td>
                </tr>
                <tr>
                    <th>AFTER</th>
                    <th>AFTER</th>
                </tr>
                <tr>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['RECTIFIER_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['BATTERY_RACK_1_OPEN'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Rectifier System with Module/Load</b></td>
                    <td class="text-center"><b>Battery After Swap</b></td>
                </tr>
            </tbody>
        </table>


        <p>&nbsp;</p>
        @if ( ! empty($report_data['IS_OLD_CABLE_ENOUGH']) )
        <p><b>Note:</b> Kabel lama cukup untuk instalasi semua battery baru.</p>
        @else
        <p><b>Note:</b> Kabel lama tidak cukup untuk instalasi semua battery baru.</p>
        @endif
        <table class="table table-bordered table-center-heading">
            <tbody>
                <tr>
                    <td class="text-center" width="50%"><img src="{{ $report_data['IMAGES']['OLD_CABLE_ON_RACK'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                    <td class="text-center"><img src="{{ $report_data['IMAGES']['OLD_CABLE_ON_MCB'] or 'img/no_image.jpg' }}" class="doc-image"></td>
                </tr>
                <tr>
                    <td class="text-center"><b>Foto kabel di rack battery</b></td>
                    <td class="text-center"><b>Foto kabel koneksi di terminal mcb battery</b></td>
                </tr>
            </tbody>
        </table>













    </div>









</body>
</html>

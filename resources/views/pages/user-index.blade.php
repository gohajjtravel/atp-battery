@extends('layouts.dashboard')



@section('page-title')
@lang('pages.user.users')
@endsection



@section('page-style')
<!-- DataTables -->
<!-- <link rel="stylesheet" href="{{ asset('vendors/datatables/jquery.dataTables.min.css') }}"> -->
<link rel="stylesheet" href="{{ asset('vendors/datatables/dataTables.bootstrap.css') }}">
@endsection



@section('body-class')
 sidebar-mini
@endsection



@section('page-content')
<div class="wrapper">


    @include('layouts.header')


    @include('layouts.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @lang('pages.user.users')
                &nbsp;&nbsp;<button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add</button>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> @lang('pages.common.home')</a></li>
                <li class="active">@lang('pages.user.users')</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @include('layouts.flash')

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-condensed table-hover" id="users-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>@lang('pages.user.name')</th>
                                        <th>@lang('pages.user.email')</th>
                                        <th>@lang('pages.user.role')</th>
                                        <!-- <th>@lang('pages.user.dashboard-access')</th> -->
                                        <th>@lang('pages.common.actions')</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.footer')

</div>
<!-- ./wrapper -->
@endsection



@section('modals')
<div class="modal fade" id="addModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.user.add-modal.title')</h4>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal" action="{{ route('user.add') }}">
                    <div class="form-group">
                        <label for="inputAddUserName" class="col-sm-4 control-label">@lang('pages.user.name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAddUserName" placeholder="@lang('pages.user.name')" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserEmail" class="col-sm-4 control-label">@lang('pages.user.email')</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputAddUserEmail" placeholder="@lang('pages.user.email')" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserPassword" class="col-sm-4 control-label">@lang('pages.user.password')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputAddUserPassword" placeholder="@lang('pages.user.password')" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserPasswordConfirm" class="col-sm-4 control-label">@lang('pages.user.password-confirm')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputAddUserPasswordConfirm" placeholder="@lang('pages.user.password-confirm')" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.user.role')</label>
                        <div class="col-sm-8 radio-custom">
                            <label>
                                <input type="radio" name="role" class="minimal" value="staff" checked>
                                Staff
                            </label>
                            <label>
                                <input type="radio" name="role" class="minimal" value="administrator">
                                Administrator
                            </label>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.user.dashboard-access')</label>
                        <div class="col-sm-8 radio-custom">
                            <label>
                                <input type="radio" name="dashboard_access" class="minimal" value="0" checked>
                                No
                            </label>
                            <label>
                                <input type="radio" name="dashboard_access" class="minimal" value="1">
                                Yes
                            </label>
                        </div>
                    </div> -->

                    {{ csrf_field() }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.add-modal.close-button')</button>
                <button type="button" class="btn btn-primary submit-form-button">@lang('pages.common.add-modal.save-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.user.edit-modal.title')</h4>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal" action="{{ route('user.update') }}">
                    <div class="form-group">
                        <label for="inputEditUserName" class="col-sm-4 control-label">@lang('pages.user.name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEditUserName" placeholder="@lang('pages.user.name')" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEditUserEmail" class="col-sm-4 control-label">@lang('pages.user.email')</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputEditUserEmail" placeholder="@lang('pages.user.email')" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEditUserPassword" class="col-sm-4 control-label">@lang('pages.user.password')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputEditUserPassword" placeholder="@lang('pages.user.password')" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEditUserPasswordConfirm" class="col-sm-4 control-label">@lang('pages.user.password-confirm')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputEditUserPasswordConfirm" placeholder="@lang('pages.user.password-confirm')" name="password_confirmation">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.user.role')</label>
                        <div class="col-sm-8 radio-custom">
                            <label>
                                <input type="radio" name="role" class="minimal" value="staff" checked>
                                Staff
                            </label>
                            <label>
                                <input type="radio" name="role" class="minimal" value="administrator">
                                Administrator
                            </label>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.user.dashboard-access')</label>
                        <div class="col-sm-8 radio-custom">
                            <label>
                                <input type="radio" name="dashboard_access" class="minimal" value="0" checked>
                                No
                            </label>
                            <label>
                                <input type="radio" name="dashboard_access" class="minimal" value="1">
                                Yes
                            </label>
                        </div>
                    </div> -->
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.edit-modal.close-button')</button>
                <button type="button" class="btn btn-primary submit-form-button">@lang('pages.common.edit-modal.save-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.user.delete-modal.title')</h4>
            </div>
            <div class="modal-body">
                <div class="callout callout-danger">
                    <i class="fa fa-exclamation-triangle "></i>&nbsp;&nbsp;@lang('pages.common.delete-message')
                </div>
                <form method="post" class="form-horizontal" action="{{ route('user.destroy') }}">
                    <div class="form-group">
                        <label for="inputEditUserName" class="col-sm-4 control-label">@lang('pages.user.name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEditUserName" placeholder="@lang('pages.user.name')" name="name" readonly="readonly">
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.delete-modal.close-button')</button>
                <button type="button" class="btn btn-danger submit-form-button">@lang('pages.common.delete-modal.delete-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection



@section('javascripts')
<!-- DataTables -->
<script src="{{ asset('vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
$(function () {

    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('user.data') !!}',
        columns: [
            { data: 'hashed_id', name: 'hashed_id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'role_display_name', name: 'role_display_name' },
            // { data: 'dashboard_access' },
            { data: 'actions', name: 'actions', sortable: false }
        ]
    });

    $('#addModal, #editModal, #deleteModal').on('click', '.submit-form-button', function() {
        var theForm = $(this).parents('.modal-content').find('form');
        // TODO: need to validate the form
        theForm.submit();
    });

    $('#editModal, #deleteModal').on('show.bs.modal', function (event) {
        if (event.namespace == "bs.modal") {

            var button = $(event.relatedTarget);
            var modal = $(this);
            var itemId = button.data('id');

            $.get('{{ route("user.show") }}', {
                id: itemId
            })
            .done(function( data ) {
                modal.find('.form-control').each(function() {
                    $(this).val('');
                });

                modal.find('input[name="id"]').val(itemId);
                modal.find('input[name="name"]').val(data.name);
                modal.find('input[name="email"]').val(data.email);
                modal.find('input[name="role"]').iCheck('uncheck');
                modal.find('input[value="'+data.roles[0].name+'"]').iCheck('check');
                // modal.find('input[value="'+data.is_active+'"]').iCheck('check');
            })
            .fail(function() {
            });
        }
    });
});

</script>
@endsection

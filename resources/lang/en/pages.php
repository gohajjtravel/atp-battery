<?php

return [
    'user' => [
        'users' => 'Users',
        'name' => 'Name',
        'email' => 'Email',
        'role' => 'Role',
        'password' => 'Password',
        'dashboard-access' => 'Dashboard Access',
        'password-confirm' => 'Password Confirmation',
        'edit-modal' => [
            'title' => 'Edit User',
        ],
        'add-modal' => [
            'title' => 'Add User',
        ],
        'delete-modal' => [
            'title' => 'Delete User',
        ],
    ],
    'common' => [
        'home' => 'Home',
        'actions' => 'Actions',
        'edit-modal' => [
            'close-button' => 'Close',
            'save-button' => 'Save Changes',
        ],
        'add-modal' => [
            'close-button' => 'Close',
            'save-button' => 'Add',
        ],
        'delete-modal' => [
            'close-button' => 'Cancel',
            'delete-button' => 'Delete',
        ],
        'delete-message' => '<strong>Warning!</strong> Are you sure to continue with deletion?',
    ],
    'form-data' => [
        'title' => 'Form Data',
        'staff_name' => 'Staff Name',
        'site_name' => 'Site Name',
        'submitted_at' => 'Submitted At',
        'edit-modal' => [
            'title' => 'Edit Form Data',
        ],
        'delete-modal' => [
            'title' => 'Delete Form Data',
        ],
        'approve-modal' => [
            'title' => 'Approve Form Data',
            'approve-button' => 'Approve',
        ],
        'approve-message' => '<strong>Attention!</strong> Are you sure to continue with approval?',
    ],
    'report' => [
        'index' => 'Reports',
        'edit' => 'Edit Report',
        'ewo_nbwo' => 'EWO/NBWO',
        'submit_date' => 'Tanggal kirim',
        'old_battery_count' => 'Jumlah blok baterai lama',
        'old_battery_cage' => 'Kerangkeng baterai lama',
        'install_on_same_rack' => 'Install di rak rectifier',
        'is_old_cable_enough' => 'Kabel lama cukup',
        'rectifier_module_count' => 'Jumlah modul rectifier',
    ],
];

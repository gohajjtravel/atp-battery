<?php

use Illuminate\Database\Seeder;

use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'administrator';
        $role->display_name = 'Administrator';
        $role->description = 'Website Administrator';
        $role->save();

        $role = new Role();
        $role->name = 'staff';
        $role->display_name = 'Staff';
        $role->description = 'Company Staff';
        $role->save();
    }
}

<?php

namespace App\Mail;


use App\FormData;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $form_data;

    public $tries = 1;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FormData $form_data)
    {
        $this->form_data = $form_data;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $parameters = json_decode($this->form_data->parameters, true);

        return $this->subject('Form Approved - Site '.$parameters['SITE_NAME'])
                    ->view('emails.form_approved')
                    // ->attach($this->form_data->report_path, [
                    //         'as' => 'report.pdf',
                    //         'mime' => 'application/pdf',
                    //     ])
                    ;
    }
}

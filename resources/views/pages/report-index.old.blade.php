@extends('layouts.dashboard')



@section('page-title')
@lang('pages.report.index')
@endsection



@section('page-style')
<style>
canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}
</style>
@endsection



@section('body-class')
 sidebar-mini
@endsection



@section('page-content')
<div class="wrapper">


    @include('layouts.header')


    @include('layouts.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @lang('pages.report.index')
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> @lang('pages.common.home')</a></li>
                <li class="active">@lang('pages.report.index')</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @include('layouts.flash')

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <div class="box-body">

                            <div style="width:99%;">
                                <canvas id="canvas"></canvas>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.footer')

</div>
<!-- ./wrapper -->
@endsection



@section('modals')
<div class="modal fade" id="addModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('pages.user.add-modal.title')</h4>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal" action="{{ route('user.add') }}">
                    <div class="form-group">
                        <label for="inputAddUserName" class="col-sm-4 control-label">@lang('pages.user.name')</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAddUserName" placeholder="@lang('pages.user.name')" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserEmail" class="col-sm-4 control-label">@lang('pages.user.email')</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputAddUserEmail" placeholder="@lang('pages.user.email')" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserPassword" class="col-sm-4 control-label">@lang('pages.user.password')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputAddUserPassword" placeholder="@lang('pages.user.password')" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddUserPasswordConfirm" class="col-sm-4 control-label">@lang('pages.user.password-confirm')</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputAddUserPasswordConfirm" placeholder="@lang('pages.user.password-confirm')" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">@lang('pages.user.role')</label>
                        <div class="col-sm-8 radio-custom">
                            <label>
                                <input type="radio" name="role" class="minimal" value="staff" checked>
                                Staff
                            </label>
                            <label>
                                <input type="radio" name="role" class="minimal" value="administrator">
                                Administrator
                            </label>
                        </div>
                    </div>

                    {{ csrf_field() }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('pages.common.add-modal.close-button')</button>
                <button type="button" class="btn btn-primary submit-form-button">@lang('pages.common.add-modal.save-button')</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




@endsection



@section('javascripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script src="{{ asset('js/utils.js') }}"></script>

<script>
$(function () {

    // var timeFormat = 'MM/DD/YYYY HH:mm';
    var timeFormat = 'YYYY-MM-DD';

    function newDate(days) {
        return moment().add(days, 'd').toDate();
    }

    function newDateString(days) {
        return moment().add(days, 'd').format(timeFormat);
    }

    function respondCanvas(submittedData, approvedData) {
        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                // labels: [ // Date Objects
                //     newDate(0),
                //     newDate(1),
                //     newDate(2),
                //     newDate(3),
                //     newDate(4),
                //     newDate(5),
                //     newDate(6)
                // ],
                datasets: [{
                //     label: 'My First dataset',
                //     backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                //     borderColor: window.chartColors.red,
                //     fill: false,
                //     data: [
                //         randomScalingFactor(),
                //         randomScalingFactor(),
                //         randomScalingFactor(),
                //         randomScalingFactor(),
                //         randomScalingFactor(),
                //         randomScalingFactor(),
                //         randomScalingFactor()
                //     ],
                // }, {
                    label: 'Approved Forms',
                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.blue,
                    fill: false,
                    data: approvedData,
                }, {
                    label: 'Submitted Forms',
                    backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.green,
                    fill: false,
                    data: submittedData,
                }]
            },
            options: {
                responsive: true,
                title: {
                    text: 'Form Data Reports'
                },
                tooltips: {
                    mode: 'x',
                    // axis: 'x',
                    // intersect: true,
                },
                // hover: {
                //     mode: 'nearest',
                //     intersect: true
                // },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            parser: timeFormat,
                            // round: 'day'
                            tooltipFormat: 'll'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'value'
                        }
                    }]
                },
            }
        };

        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);

    };

    var GetChartData = function () {
        $.ajax({
            url: "{{ route('report.data') }}",
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                respondCanvas(data.all, data.approved);
            }
        });
    };

    GetChartData();

    $('#addModal, #editModal, #deleteModal').on('click', '.submit-form-button', function() {
        var theForm = $(this).parents('.modal-content').find('form');
        // TODO: need to validate the form
        theForm.submit();
    });

    $('#editModal, #deleteModal').on('show.bs.modal', function (event) {
        if (event.namespace == "bs.modal") {

            var button = $(event.relatedTarget);
            var modal = $(this);
            var itemId = button.data('id');

            $.get('{{ route("user.show") }}', {
                id: itemId
            })
            .done(function( data ) {
                modal.find('.form-control').each(function() {
                    $(this).val('');
                });

                modal.find('input[name="id"]').val(itemId);
                modal.find('input[name="name"]').val(data.name);
                modal.find('input[name="email"]').val(data.email);
                modal.find('input[name="role"]').iCheck('uncheck');
                modal.find('input[value="'+data.roles[0].name+'"]').iCheck('check');
            })
            .fail(function() {
            });
        }
    });
});

</script>
@endsection

<?php 

return [
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'please-login' => 'Sign in to start your session',
    'button' => [
        'sign-in' => 'Sign In',
    ],
];
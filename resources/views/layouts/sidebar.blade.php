<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        @if (Auth::check())
        <div class="user-panel">
            <div class="pull-left image">
                @if (Auth::user()->avatar_url == "")
                <img src="{{ asset('img/default-avatar.jpg') }}" class="img-circle" alt="User Image">
                @else
                <img src="{{ asset(Auth::user()->avatar_url) }}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <span class="user-role">{{ Auth::user()->role_display_name }}</span>
            </div>
        </div>
        @endif
        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="@lang('layouts.sidebar.search-menu')">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">@lang('layouts.sidebar.main-navigation')</li>
            <li class="{{ Request::is('user') ? 'active' : '' }}">
                <a href="{{ route('user.index') }}">
                    <i class="im im-users"></i> <span>@lang('layouts.sidebar.users')</span>
                </a>
            </li>

            <!--
            <li class="{{ Request::is('form-data') ? 'active' : '' }}">
                <a href="{{ route('form-data.index') }}">
                    <i class="im im-data-validate"></i> <span>@lang('layouts.sidebar.form-data')</span>
                </a>
            </li>
            -->


            <li class="{{ Request::is('report') ? 'active' : '' }}">
                <a href="{{ route('report.index') }}">
                    <i class="im im-line-chart-up"></i> <span>@lang('layouts.sidebar.report')</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

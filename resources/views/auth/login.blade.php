@extends('layouts.dashboard')


@section('page-title')
Login
@endsection


@section('body-class') login-page
@endsection


@section('page-content')

<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" width="175"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">@lang('auth.please-login')</p>

        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <!-- <div class="col-xs-8">

                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div> -->
                <!-- /.col -->
                <!-- <div class="col-xs-4"> -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('auth.button.sign-in')</button>
                </div>
                <!-- /.col -->
            </div>
        </form>


        <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
            Forgot Your Password?
        </a> -->
        <!-- <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a> -->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection

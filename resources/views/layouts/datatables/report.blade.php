@if ($report_path)
<a target="_blank" href="{{ asset($report_path) }}" role="button" class="btn btn-primary btn-xs">View</a>
<button type="button" class="btn btn-primary btn-xs edit-data-button" data-id="{{ $hashed_id }}" data-toggle="modal" data-target="#approveModal">Approve</button>
@endif

<?php

return [
    'user_created_successfully' => 'User created successfully',
    'user_updated_successfully' => 'User updated successfully',
    'user_deleted_successfully' => 'User deleted successfully',
    'form_data_deleted_successfully' => 'Form data deleted successfully',
    'form_data_approved_successfully' => 'Form data approved successfully',
];

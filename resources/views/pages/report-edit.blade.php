@extends('layouts.dashboard')



@section('page-title')
@lang('pages.report.index')
@endsection



@section('page-style')
<!-- DataTables -->
<!-- <link rel="stylesheet" href="{{ asset('vendors/datatables/jquery.dataTables.min.css') }}"> -->
<link rel="stylesheet" href="{{ asset('vendors/datatables/dataTables.bootstrap.css') }}">
@endsection



@section('body-class')
 sidebar-mini
@endsection



@section('page-content')
<div class="wrapper">


    @include('layouts.header')


    @include('layouts.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @lang('pages.report.edit') &nbsp;&nbsp;&nbsp;
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> @lang('pages.common.home')</a></li>
                <li><a href="{{ route('report.index') }}">@lang('pages.report.index')</a></li>
                <li class="active">{{ $form_data->site_name }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @include('layouts.flash')

            <form action="" id="input_form">
                <input type="hidden" name="id" value="{{ $form_data->hashed_id }}">
                <h3><strong>Site Identification</strong></h3>
                <hr>
                <div class="card card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="site_id">Site ID</label>
                                <input type="text" class="form-control" id="site_id" name="SITE_ID" value="{{ $report_data['SITE_ID'] or '' }}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="site_name">Site Name</label>
                                <input type="text" class="form-control" id="site_name" name="SITE_NAME" value="{{ $report_data['SITE_NAME'] or '' }}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="site_address">Site Address</label>
                                <input type="text" class="form-control" id="site_address" name="SITE_ADDRESS" value="{{ $report_data['SITE_ADDRESS'] or '' }}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="installer_name">Installer Name</label>
                                <input type="text" class="form-control" id="installer_name" name="INSTALLER_NAME" value="{{ $report_data['INSTALLER_NAME'] or '' }}">
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="email_person">Email Person in Charge</label>
                                <input type="text" class="form-control" id="email_person" name="EMAIL" value="{{ $report_data['EMAIL'] or '' }}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="nbwo">No. NBWO / EWO</label>
                                <input type="text" class="form-control" id="nbwo" name="NBWO_EWO_NUMBER" value="{{ $report_data['NBWO_EWO_NUMBER'] or '' }}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="no_po">No. PO</label>
                                <input type="text" class="form-control" id="no_po" name="PO_NUMBER" value="{{ $report_data['PO_NUMBER'] or '' }}">
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div>
                    <h3><strong>Old Battery Condition</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label for="merk">Merk</label>
                                    <input type="text" class="form-control" id="merk" name="OLD_BATTERY[BRAND]" value="{{ $report_data['OLD_BATTERY']['BRAND'] or '' }}">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="qty">Quantity</label>
                                    <input type="text" class="form-control" id="qty" name="OLD_BATTERY[QTY]" value="{{ $report_data['OLD_BATTERY']['QTY'] or '' }}">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Apakah ada cage/kerangkeng di battery yang lama?</label>
                                    @if ($report_data['OLD_BATTERY']['CAGE'] == 1)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="cage1" name="OLD_BATTERY[CAGE]" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="cage1">Ada</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="cage2" name="OLD_BATTERY[CAGE]" value="0" class="custom-control-input">
                                        <label class="custom-control-label" for="cage2">Tidak Ada</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="cage1" name="OLD_BATTERY[CAGE]" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="cage1">Ada</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="cage2" name="OLD_BATTERY[CAGE]" value="0" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="cage2">Tidak Ada</label>
                                    </div>
                                    @endif
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="rectifier_module_count">Jumlah Modul Rectifier</label>
                                    <input type="text" class="form-control" id="rectifier_module_count" name="RECTIFIER_MODULE_COUNT" value="{{ $report_data['RECTIFIER_MODULE_COUNT'] or '' }}">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="rectifier_module_count">Apakah kabel lama cukup untuk semua battery baru?</label>
                                    @if (isset($report_data['IS_OLD_CABLE_ENOUGH']) AND ($report_data['IS_OLD_CABLE_ENOUGH'] == 1))
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="old_cable_enough1" name="IS_OLD_CABLE_ENOUGH" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="old_cable_enough1">Cukup</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="old_cable_enough2" name="IS_OLD_CABLE_ENOUGH" value="0" class="custom-control-input">
                                        <label class="custom-control-label" for="old_cable_enough2">Tidak Cukup</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="old_cable_enough1" name="IS_OLD_CABLE_ENOUGH" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="old_cable_enough1">Cukup</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="old_cable_enough2" name="IS_OLD_CABLE_ENOUGH" value="0" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="old_cable_enough2">Tidak Cukup</label>
                                    </div>
                                    @endif
                                </fieldset>

                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Foto Rectifier Lama (Tampak utuh)</label>
                                    <input type="file" class="form-control input-image" name="OLD_RECTIFIER"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['OLD_RECTIFIER']) ? asset($report_data['IMAGES']['OLD_RECTIFIER']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Foto Battery Lama (Tampak utuh)</label>
                                    <input type="file" class="form-control input-image" name="OLD_BATTERY_IMAGE"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['OLD_BATTERY']) ? asset($report_data['IMAGES']['OLD_BATTERY']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Foto Rectifier Module</label>
                                    <input type="file" class="form-control input-image" name="RECTIFIER_MODULE"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['RECTIFIER_MODULE']) ? asset($report_data['IMAGES']['RECTIFIER_MODULE']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Foto kabel di rack battery (terpotong/tidak ada)</label>
                                    <input type="file" class="form-control input-image" name="OLD_CABLE_ON_RACK"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['OLD_CABLE_ON_RACK']) ? asset($report_data['IMAGES']['OLD_CABLE_ON_RACK']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Foto kabel koneksi di terminal mcb battery</label>
                                    <input type="file" class="form-control input-image" name="OLD_CABLE_ON_MCB"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['OLD_CABLE_ON_MCB']) ? asset($report_data['IMAGES']['OLD_CABLE_ON_MCB']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <h3><strong>New Battery Condition</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Pilih tipe battery yang akan dipasang</label>
                                    @if (($report_data['BATTERY_TYPE'] == 'ICS') OR ($report_data['BATTERY_TYPE'] == 'ICS_150'))
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt1" name="BATTERY_TYPE" value="ICS_150" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="type-batt1">ICS 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt2" name="BATTERY_TYPE" value="NDT_150" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt2">NDT 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt3" name="BATTERY_TYPE" value="NDT_100" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt3">NDT 100</label>
                                    </div>
                                    @elseif (($report_data['BATTERY_TYPE'] == 'NDT') OR ($report_data['BATTERY_TYPE'] == 'NDT_150'))
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt1" name="BATTERY_TYPE" value="ICS_150" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt1">ICS 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt2" name="BATTERY_TYPE" value="NDT_150" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="type-batt2">NDT 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt3" name="BATTERY_TYPE" value="NDT_100" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt3">NDT 100</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt1" name="BATTERY_TYPE" value="ICS_150" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt1">ICS 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt2" name="BATTERY_TYPE" value="NDT_150" class="custom-control-input">
                                        <label class="custom-control-label" for="type-batt2">NDT 150</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type-batt3" name="BATTERY_TYPE" value="NDT_100" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="type-batt3">NDT 100</label>
                                    </div>
                                    @endif
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Jumlah Blok Battery</label>
                                    <select name="BATTERY_QTY" id="new_battery_count" class="custom-select">
                                        <option value="4" {{ $report_data['BATTERY_QTY'] == '4' ? 'selected' : '' }}>4</option>
                                        <option value="8" {{ $report_data['BATTERY_QTY'] == '8' ? 'selected' : '' }}>8</option>
                                        <option value="12" {{ $report_data['BATTERY_QTY'] == '12' ? 'selected' : '' }}>12</option>
                                        <option value="16" {{ $report_data['BATTERY_QTY'] == '16' ? 'selected' : '' }}>16</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Apakah battery baru ini akan dipasang di rack yang sama dengan rectifier?</label>
                                    @if ($report_data['INSTALL_ON_SAME_RACK'] == '1')
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="new-batt1" name="INSTALL_ON_SAME_RACK" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="new-batt1">Sama Rack</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="new-batt2" name="INSTALL_ON_SAME_RACK" value="0" class="custom-control-input">
                                        <label class="custom-control-label" for="new-batt2">Beda Rack</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="new-batt1" name="INSTALL_ON_SAME_RACK" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="new-batt1">Sama Rack</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="new-batt2" name="INSTALL_ON_SAME_RACK" value="0" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="new-batt2">Beda Rack</label>
                                    </div>
                                    @endif
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <h3><strong>New Battery Blok</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="new_battery_bank_1">
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 1 Battery Baru (Tampak utuh)</label>
                                                <input type="file" class="form-control input-image" name="BATT_1_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_1_SN']) ? asset($report_data['IMAGES']['BATT_1_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat1">Masukkan Serial Number blok 1 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat1" name="NEW_BATTERY[BATT_1][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 2 Battery Baru (Tampak utuh)</label>
                                                <input type="file" class="form-control input-image" name="BATT_2_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_2_SN']) ? asset($report_data['IMAGES']['BATT_2_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat2">Masukkan Serial Number blok 2 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat2" name="NEW_BATTERY[BATT_2][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 3 Battery Baru (Tampak utuh)</label>
                                                <input type="file" class="form-control input-image" name="BATT_3_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_3_SN']) ? asset($report_data['IMAGES']['BATT_3_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat3">Masukkan Serial Number blok 3 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat3" name="NEW_BATTERY[BATT_3][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 4 Battery Baru (Tampak utuh)</label>
                                                <input type="file" class="form-control input-image" name="BATT_4_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_4_SN']) ? asset($report_data['IMAGES']['BATT_4_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat4">Masukkan Serial Number blok 4 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat4" name="NEW_BATTERY[BATT_4][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['SN'] }}">
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="new_battery_bank_2">
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 5 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_5_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_5_SN']) ? asset($report_data['IMAGES']['BATT_5_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat5">Masukkan Serial Number blok 5 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat5" name="NEW_BATTERY[BATT_5][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 6 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_6_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_6_SN']) ? asset($report_data['IMAGES']['BATT_6_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat6">Masukkan Serial Number blok 6 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat6" name="NEW_BATTERY[BATT_6][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 7 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_7_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_7_SN']) ? asset($report_data['IMAGES']['BATT_7_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat7">Masukkan Serial Number blok 7 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat7" name="NEW_BATTERY[BATT_7][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 8 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_8_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_8_SN']) ? asset($report_data['IMAGES']['BATT_8_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat8">Masukkan Serial Number blok 8 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat8" name="NEW_BATTERY[BATT_8][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['SN'] }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="new_battery_bank_3">
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 9 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_9_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_9_SN']) ? asset($report_data['IMAGES']['BATT_9_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat9">Masukkan Serial Number blok 9 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat9" name="NEW_BATTERY[BATT_9][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 10 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_10_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_10_SN']) ? asset($report_data['IMAGES']['BATT_10_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat10">Masukkan Serial Number blok 10 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat10" name="NEW_BATTERY[BATT_10][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 11 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_11_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_11_SN']) ? asset($report_data['IMAGES']['BATT_11_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat11">Masukkan Serial Number blok 11 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat11" name="NEW_BATTERY[BATT_11][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 12 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_12_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_12_SN']) ? asset($report_data['IMAGES']['BATT_12_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat12">Masukkan Serial Number blok 12 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat12" name="NEW_BATTERY[BATT_12][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['SN'] }}">
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="new_battery_bank_4">
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 13 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_13_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_13_SN']) ? asset($report_data['IMAGES']['BATT_13_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat13">Masukkan Serial Number blok 13 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat13" name="NEW_BATTERY[BATT_13][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 14 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_14_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_14_SN']) ? asset($report_data['IMAGES']['BATT_14_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat14">Masukkan Serial Number blok 14 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat14" name="NEW_BATTERY[BATT_14][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 15 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_15_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_15_SN']) ? asset($report_data['IMAGES']['BATT_15_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat15">Masukkan Serial Number blok 15 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat15" name="NEW_BATTERY[BATT_15][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['SN'] }}">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Foto Serial Number Blok 16 Battery Baru (Tampak utuh)</label>

                                                <input type="file" class="form-control input-image" name="BATT_16_SN"></input>
                                                <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_16_SN']) ? asset($report_data['IMAGES']['BATT_16_SN']) : asset('img/no_image.jpg') }}"></div>
                                                <br>
                                                <label for="sn_bat16">Masukkan Serial Number blok 16 battery baru</label>
                                                <input type="text" class="form-control" id="sn_bat16" name="NEW_BATTERY[BATT_16][SN]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['SN'] }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3><strong>Pengukuran</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label for="last_name">Tegangan PLN</label>
                                    <input type="text" class="form-control" name="AC_INPUT" value="{{ $report_data['AC_INPUT'] }}">
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Jumlah Fase PLN</label>
                                    @if ($report_data['INPUT_PHASE'] == 3)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fase_pln1" name="INPUT_PHASE" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="fase_pln1">1</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fase_pln2" name="INPUT_PHASE" value="3" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="fase_pln2">3</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fase_pln1" name="INPUT_PHASE" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="fase_pln1">1</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fase_pln2" name="INPUT_PHASE" value="3" class="custom-control-input">
                                        <label class="custom-control-label" for="fase_pln2">3</label>
                                    </div>
                                    @endif
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Tegangan output rectifier / Float Voltage</label>
                                    <input type="text" class="form-control" name="FLOAT_VOLTAGE" value="{{ $report_data['FLOAT_VOLTAGE'] }}">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <h3><strong>Pengetesan Fungsi Rectifier</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Tes Battery Fuse Alarm</label>
                                    @if ($report_data['ALARM_BATTERY_FUSE_FAIL'] == 1)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fuse_alarm1" name="ALARM_BATTERY_FUSE_FAIL" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="fuse_alarm1">Pass</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fuse_alarm2" name="ALARM_BATTERY_FUSE_FAIL" value="0" class="custom-control-input">
                                        <label class="custom-control-label" for="fuse_alarm2">Fail</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fuse_alarm1" name="ALARM_BATTERY_FUSE_FAIL" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="fuse_alarm1">Pass</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="fuse_alarm2" name="ALARM_BATTERY_FUSE_FAIL" value="0" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="fuse_alarm2">Fail</label>
                                    </div>
                                    @endif
                                </fieldset>
                                <fieldset class="form-group">
                                    <label>Konektifitas NMS/OSS</label>
                                    @if ($report_data['ALARM_BATTERY_FUSE_FAIL'] == 1)
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="nms1" name="NMM_OSS_POWER" value="1" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="nms1">Connect</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="nms2" name="NMM_OSS_POWER" value="0" class="custom-control-input">
                                        <label class="custom-control-label" for="nms2">Not Connected</label>
                                    </div>
                                    @else
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="nms1" name="NMM_OSS_POWER" value="1" class="custom-control-input">
                                        <label class="custom-control-label" for="nms1">Connect</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="nms2" name="NMM_OSS_POWER" value="0" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="nms2">Not Connected</label>
                                    </div>
                                    @endif
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <label>Foto Battery Fuse Fail</label>
                                    <input type="file" class="form-control input-image" name="BATTERY_FUSE_FAIL"></input>
                                    <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_FUSE_FAIL']) ? asset($report_data['IMAGES']['BATTERY_FUSE_FAIL']) : asset('img/no_image.jpg') }}"></div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <h3><strong>Pengetesan Battery</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="last_name">Catat Arus Beban Rectifier (Ampere)</label>
                                            <input type="text" class="form-control" id="arus_rect" name="LOAD_CURRENT" value="{{ $report_data['LOAD_CURRENT'] }}">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="last_name">Catat Suhu (Celcius)</label>
                                            <input type="text" class="form-control" id="suhu" name="TEMPERATURE" value="{{ $report_data['TEMPERATURE'] }}">
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="last_name">Pencatatan Menit ke 0</label>
                                            <div class=""></div>
                                            <div class="new_battery_bank_1">
                                                <label>Blok 1</label>
                                                <input type="text" class="form-control" id="blok0100" name="NEW_BATTERY[BATT_1][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['T0'] }}">
                                                <br>
                                                <label>Blok 2</label>
                                                <input type="text" class="form-control" id="blok0200" name="NEW_BATTERY[BATT_2][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['T0'] }}">
                                                <br>
                                                <label>Blok 3</label>
                                                <input type="text" class="form-control" id="blok0300" name="NEW_BATTERY[BATT_3][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['T0'] }}">
                                                <br>
                                                <label>Blok 4</label>
                                                <input type="text" class="form-control" id="blok0400" name="NEW_BATTERY[BATT_4][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['T0'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_2">
                                                <label>Blok 5</label>
                                                <input type="text" class="form-control" id="blok0500" name="NEW_BATTERY[BATT_5][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['T0'] }}">
                                                <br>
                                                <label>Blok 6</label>
                                                <input type="text" class="form-control" id="blok0600" name="NEW_BATTERY[BATT_6][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['T0'] }}">
                                                <br>
                                                <label>Blok 7</label>
                                                <input type="text" class="form-control" id="blok0700" name="NEW_BATTERY[BATT_7][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['T0'] }}">
                                                <br>
                                                <label>Blok 8</label>
                                                <input type="text" class="form-control" id="blok0800" name="NEW_BATTERY[BATT_8][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['T0'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_3">
                                                <label>Blok 9</label>
                                                <input type="text" class="form-control" id="blok9000" name="NEW_BATTERY[BATT_9][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['T0'] }}">
                                                <br>
                                                <label>Blok 10</label>
                                                <input type="text" class="form-control" id="blok1000" name="NEW_BATTERY[BATT_10][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['T0'] }}">
                                                <br>
                                                <label>Blok 11</label>
                                                <input type="text" class="form-control" id="blok1100" name="NEW_BATTERY[BATT_11][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['T0'] }}">
                                                <br>
                                                <label>Blok 12</label>
                                                <input type="text" class="form-control" id="blok1200" name="NEW_BATTERY[BATT_12][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['T0'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_4">
                                                <label>Blok 13</label>
                                                <input type="text" class="form-control" id="blok1300" name="NEW_BATTERY[BATT_13][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['T0'] }}">
                                                <br>
                                                <label>Blok 14</label>
                                                <input type="text" class="form-control" id="blok1400" name="NEW_BATTERY[BATT_14][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['T0'] }}">
                                                <br>
                                                <label>Blok 15</label>
                                                <input type="text" class="form-control" id="blok1500" name="NEW_BATTERY[BATT_15][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['T0'] }}">
                                                <br>
                                                <label>Blok 16</label>
                                                <input type="text" class="form-control" id="blok1600" name="NEW_BATTERY[BATT_16][T0]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['T0'] }}">
                                                <br>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="blok">Pencatatan Menit ke 15</label>
                                            <div class="new_battery_bank_1">
                                                <label>Blok 1</label>
                                                <input type="text" class="form-control" id="blok0115" name="NEW_BATTERY[BATT_1][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['T1'] }}">
                                                <br>
                                                <label>Blok 2</label>
                                                <input type="text" class="form-control" id="blok0215" name="NEW_BATTERY[BATT_2][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['T1'] }}">
                                                <br>
                                                <label>Blok 3</label>
                                                <input type="text" class="form-control" id="blok0315" name="NEW_BATTERY[BATT_3][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['T1'] }}">
                                                <br>
                                                <label>Blok 4</label>
                                                <input type="text" class="form-control" id="blok0415" name="NEW_BATTERY[BATT_4][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['T1'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_2">
                                                <label>Blok 5</label>
                                                <input type="text" class="form-control" id="blok0515" name="NEW_BATTERY[BATT_5][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['T1'] }}">
                                                <br>
                                                <label>Blok 6</label>
                                                <input type="text" class="form-control" id="blok0615" name="NEW_BATTERY[BATT_6][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['T1'] }}">
                                                <br>
                                                <label>Blok 7</label>
                                                <input type="text" class="form-control" id="blok0715" name="NEW_BATTERY[BATT_7][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['T1'] }}">
                                                <br>
                                                <label>Blok 8</label>
                                                <input type="text" class="form-control" id="blok0815" name="NEW_BATTERY[BATT_8][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['T1'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_3">
                                                <label>Blok 9</label>
                                                <input type="text" class="form-control" id="blok0915" name="NEW_BATTERY[BATT_9][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['T1'] }}">
                                                <br>
                                                <label>Blok 10</label>
                                                <input type="text" class="form-control" id="blok1015" name="NEW_BATTERY[BATT_10][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['T1'] }}">
                                                <br>
                                                <label>Blok 11</label>
                                                <input type="text" class="form-control" id="blok1115" name="NEW_BATTERY[BATT_11][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['T1'] }}">
                                                <br>
                                                <label>Blok 12</label>
                                                <input type="text" class="form-control" id="blok1215" name="NEW_BATTERY[BATT_12][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['T1'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_4">
                                                <label>Blok 13</label>
                                                <input type="text" class="form-control" id="blok1315" name="NEW_BATTERY[BATT_13][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['T1'] }}">
                                                <br>
                                                <label>Blok 14</label>
                                                <input type="text" class="form-control" id="blok1415" name="NEW_BATTERY[BATT_14][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['T1'] }}">
                                                <br>
                                                <label>Blok 15</label>
                                                <input type="text" class="form-control" id="blok1515" name="NEW_BATTERY[BATT_15][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['T1'] }}">
                                                <br>
                                                <label>Blok 16</label>
                                                <input type="text" class="form-control" id="blok1516" name="NEW_BATTERY[BATT_16][T1]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['T1'] }}">
                                                <br>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="blok">Pencatatan Menit ke 30</label>
                                            <div class="new_battery_bank_1">
                                                <label>Blok 1</label>
                                                <input type="text" class="form-control" id="blok0130" name="NEW_BATTERY[BATT_1][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['T2'] }}">
                                                <br>
                                                <label>Blok 2</label>
                                                <input type="text" class="form-control" id="blok0230" name="NEW_BATTERY[BATT_2][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['T2'] }}">
                                                <br>
                                                <label>Blok 3</label>
                                                <input type="text" class="form-control" id="blok0330" name="NEW_BATTERY[BATT_3][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['T2'] }}">
                                                <br>
                                                <label>Blok 4</label>
                                                <input type="text" class="form-control" id="blok0430" name="NEW_BATTERY[BATT_4][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['T2'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_2">
                                                <label>Blok 5</label>
                                                <input type="text" class="form-control" id="blok0530" name="NEW_BATTERY[BATT_5][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['T2'] }}">
                                                <br>
                                                <label>Blok 6</label>
                                                <input type="text" class="form-control" id="blok0630" name="NEW_BATTERY[BATT_6][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['T2'] }}">
                                                <br>
                                                <label>Blok 7</label>
                                                <input type="text" class="form-control" id="blok0730" name="NEW_BATTERY[BATT_7][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['T2'] }}">
                                                <br>
                                                <label>Blok 8</label>
                                                <input type="text" class="form-control" id="blok0830" name="NEW_BATTERY[BATT_8][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['T2'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_3">
                                                <label>Blok 9</label>
                                                <input type="text" class="form-control" id="blok0930" name="NEW_BATTERY[BATT_9][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['T2'] }}">
                                                <br>
                                                <label>Blok 10</label>
                                                <input type="text" class="form-control" id="blok1030" name="NEW_BATTERY[BATT_10][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['T2'] }}">
                                                <br>
                                                <label>Blok 11</label>
                                                <input type="text" class="form-control" id="blok1130" name="NEW_BATTERY[BATT_11][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['T2'] }}">
                                                <br>
                                                <label>Blok 12</label>
                                                <input type="text" class="form-control" id="blok1230" name="NEW_BATTERY[BATT_12][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['T2'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_4">
                                                <label>Blok 13</label>
                                                <input type="text" class="form-control" id="blok1330" name="NEW_BATTERY[BATT_13][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['T2'] }}">
                                                <br>
                                                <label>Blok 14</label>
                                                <input type="text" class="form-control" id="blok1430" name="NEW_BATTERY[BATT_14][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['T2'] }}">
                                                <br>
                                                <label>Blok 15</label>
                                                <input type="text" class="form-control" id="blok1530" name="NEW_BATTERY[BATT_15][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['T2'] }}">
                                                <br>
                                                <label>Blok 16</label>
                                                <input type="text" class="form-control" id="blok1630" name="NEW_BATTERY[BATT_16][T2]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['T2'] }}">
                                                <br>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="blok">Pencatatan Menit ke 45</label>
                                            <div class="new_battery_bank_1">
                                                <label>Blok 1</label>
                                                <input type="text" class="form-control" id="blok0145" name="NEW_BATTERY[BATT_1][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['T3'] }}">
                                                <br>
                                                <label>Blok 2</label>
                                                <input type="text" class="form-control" id="blok0245" name="NEW_BATTERY[BATT_2][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['T3'] }}">
                                                <br>
                                                <label>Blok 3</label>
                                                <input type="text" class="form-control" id="blok0345" name="NEW_BATTERY[BATT_3][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['T3'] }}">
                                                <br>
                                                <label>Blok 4</label>
                                                <input type="text" class="form-control" id="blok0445" name="NEW_BATTERY[BATT_4][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['T3'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_2">
                                                <label>Blok 5</label>
                                                <input type="text" class="form-control" id="blok0545" name="NEW_BATTERY[BATT_5][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['T3'] }}">
                                                <br>
                                                <label>Blok 6</label>
                                                <input type="text" class="form-control" id="blok0645" name="NEW_BATTERY[BATT_6][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['T3'] }}">
                                                <br>
                                                <label>Blok 7</label>
                                                <input type="text" class="form-control" id="blok0745" name="NEW_BATTERY[BATT_7][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['T3'] }}">
                                                <br>
                                                <label>Blok 8</label>
                                                <input type="text" class="form-control" id="blok0845" name="NEW_BATTERY[BATT_8][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['T3'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_3">
                                                <label>Blok 9</label>
                                                <input type="text" class="form-control" id="blok0945" name="NEW_BATTERY[BATT_9][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['T3'] }}">
                                                <br>
                                                <label>Blok 10</label>
                                                <input type="text" class="form-control" id="blok1045" name="NEW_BATTERY[BATT_10][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['T3'] }}">
                                                <br>
                                                <label>Blok 11</label>
                                                <input type="text" class="form-control" id="blok1145" name="NEW_BATTERY[BATT_11][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['T3'] }}">
                                                <br>
                                                <label>Blok 12</label>
                                                <input type="text" class="form-control" id="blok1245" name="NEW_BATTERY[BATT_12][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['T3'] }}">
                                                <br>
                                            </div>
                                            <div class="new_battery_bank_4">
                                                <label>Blok 13</label>
                                                <input type="text" class="form-control" id="blok1345" name="NEW_BATTERY[BATT_13][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['T3'] }}">
                                                <br>
                                                <label>Blok 14</label>
                                                <input type="text" class="form-control" id="blok1445" name="NEW_BATTERY[BATT_14][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['T3'] }}">
                                                <br>
                                                <label>Blok 15</label>
                                                <input type="text" class="form-control" id="blok1545" name="NEW_BATTERY[BATT_15][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['T3'] }}">
                                                <br>
                                                <label>Blok 16</label>
                                                <input type="text" class="form-control" id="blok1645" name="NEW_BATTERY[BATT_16][T3]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['T3'] }}">
                                                <br>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3><strong>Foto Pengukuran Diambil Setelah 1 Jam di Test</strong></h3>
                    <hr>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="new_battery_bank_1">
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 1 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_1_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_1_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_1_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0160">Masukkan Tegangan blok 1 battery baru</label>
                                        <input type="text" class="form-control" id="blok0160" name="NEW_BATTERY[BATT_1][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_1']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 2 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_2_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_2_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_2_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0260">Masukkan Tegangan blok 2 battery baru</label>
                                        <input type="text" class="form-control" id="blok0260" name="NEW_BATTERY[BATT_2][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_2']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 3 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_3_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_3_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_3_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0360">Masukkan Tegangan blok 3 battery baru</label>
                                        <input type="text" class="form-control" id="blok0360" name="NEW_BATTERY[BATT_3][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_3']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 4 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_4_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_4_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_4_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0460">Masukkan Tegangan blok 4 battery baru</label>
                                        <input type="text" class="form-control" id="blok0460" name="NEW_BATTERY[BATT_4][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_4']['T4'] }}">
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new_battery_bank_2">
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 5 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_5_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_5_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_5_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0560">Masukkan Tegangan blok 5 battery baru</label>
                                        <input type="text" class="form-control" id="blok0560" name="NEW_BATTERY[BATT_5][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_5']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 6 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_6_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_6_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_6_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0660">Masukkan Tegangan blok 6 battery baru</label>
                                        <input type="text" class="form-control" id="blok0660" name="NEW_BATTERY[BATT_6][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_6']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 7 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_7_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_7_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_7_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0760">Masukkan Tegangan blok 7 battery baru</label>
                                        <input type="text" class="form-control" id="blok0760" name="NEW_BATTERY[BATT_7][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_7']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 8 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_8_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_8_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_8_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0860">Masukkan Tegangan blok 8 battery baru</label>
                                        <input type="text" class="form-control" id="blok0860" name="NEW_BATTERY[BATT_8][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_8']['T4'] }}">
                                    </fieldset>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="new_battery_bank_3">
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 9 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_9_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_9_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_9_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok0960">Masukkan Tegangan blok 9 battery baru</label>
                                        <input type="text" class="form-control" id="blok0960" name="NEW_BATTERY[BATT_9][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_9']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 10 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_10_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_10_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_10_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1060">Masukkan Tegangan blok 10 battery baru</label>
                                        <input type="text" class="form-control" id="blok1060" name="NEW_BATTERY[BATT_10][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_10']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 11 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_11_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_11_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_11_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1160">Masukkan Tegangan blok 11 battery baru</label>
                                        <input type="text" class="form-control" id="blok1160" name="NEW_BATTERY[BATT_11][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_11']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 12 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_12_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_12_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_12_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1260">Masukkan Tegangan blok 12 battery baru</label>
                                        <input type="text" class="form-control" id="blok1260" name="NEW_BATTERY[BATT_12][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_12']['T4'] }}">
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new_battery_bank_4">
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 13 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_13_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_13_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_13_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1360">Masukkan Tegangan blok 13 battery baru</label>
                                        <input type="text" class="form-control" id="blok1360" name="NEW_BATTERY[BATT_13][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_13']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 14 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_14_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_14_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_14_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1460">Masukkan Tegangan blok 14 battery baru</label>
                                        <input type="text" class="form-control" id="blok1460" name="NEW_BATTERY[BATT_14][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_14']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 15 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_15_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_15_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_15_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1560">Masukkan Tegangan blok 15 battery baru</label>
                                        <input type="text" class="form-control" id="blok1560" name="NEW_BATTERY[BATT_15][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_15']['T4'] }}">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Foto Tegangan Blok 16 Battery Baru</label>
                                        <input type="file" class="form-control input-image" name="BATT_16_VOLTAGE"></input>
                                        <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATT_16_VOLTAGE']) ? asset($report_data['IMAGES']['BATT_16_VOLTAGE']) : asset('img/no_image.jpg') }}"></div>
                                        <br>
                                        <label for="blok1660">Masukkan Tegangan blok 16 battery baru</label>
                                        <input type="text" class="form-control" id="blok1660" name="NEW_BATTERY[BATT_16][T4]" value="{{ $report_data['NEW_BATTERY']['BATT_16']['T4'] }}">
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                    </div>
                    <h3><strong>FOTO</strong></h3>
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label>Foto Display Rectifier On Backup Battery</label>
                                            <input type="file" class="form-control input-image" name="RECTIFIER_DISPLAY_BACKUP"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['RECTIFIER_DISPLAY_BACKUP']) ? asset($report_data['IMAGES']['RECTIFIER_DISPLAY_BACKUP']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Rectifier Open</label>
                                            <input type="file" class="form-control input-image" name="RECTIFIER_OPEN"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['RECTIFIER_OPEN']) ? asset($report_data['IMAGES']['RECTIFIER_OPEN']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Rectifier Closed</label>
                                            <input type="file" class="form-control input-image" name="RECTIFIER_CLOSED"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['RECTIFIER_CLOSED']) ? asset($report_data['IMAGES']['RECTIFIER_CLOSED']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto MCB Battery</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_MCB"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_MCB']) ? asset($report_data['IMAGES']['BATTERY_MCB']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Cable Battery Installation</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_CABLE_INSTALLATION"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_CABLE_INSTALLATION']) ? asset($report_data['IMAGES']['BATTERY_CABLE_INSTALLATION']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 1 Open</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_1_OPEN"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_1_OPEN']) ? asset($report_data['IMAGES']['BATTERY_RACK_1_OPEN']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 1 Close</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_1_CLOSED"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_1_CLOSED']) ? asset($report_data['IMAGES']['BATTERY_RACK_1_CLOSED']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 2 Open (Bila ada)</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_2_OPEN"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_2_OPEN']) ? asset($report_data['IMAGES']['BATTERY_RACK_2_OPEN']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 2 Closed (Bila ada)</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_2_CLOSED"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_2_CLOSED']) ? asset($report_data['IMAGES']['BATTERY_RACK_2_CLOSED']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 3 Open (Bila ada)</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_3_OPEN"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_3_OPEN']) ? asset($report_data['IMAGES']['BATTERY_RACK_3_OPEN']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Battery Rack 3 Closed (Bila ada)</label>
                                            <input type="file" class="form-control input-image" name="BATTERY_RACK_3_CLOSED"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['BATTERY_RACK_3_CLOSED']) ? asset($report_data['IMAGES']['BATTERY_RACK_3_CLOSED']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label>Foto Display Rectifier On AC Power</label>
                                            <input type="file" class="form-control input-image" name="RECTIFIER_DISPLAY_AC"></input>
                                            <div class="preview-image"><img src="{{ isset($report_data['IMAGES']['RECTIFIER_DISPLAY_AC']) ? asset($report_data['IMAGES']['RECTIFIER_DISPLAY_AC']) : asset('img/no_image.jpg') }}"></div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <button type="submit" id="submit_button" class="btn btn-primary">Submit</button>
        </section>
    </div>

    @include('layouts.footer')

</div>
<!-- ./wrapper -->
@endsection



@section('javascripts')

    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <!-- required -->
    <script src="{{ asset('js/image-compressor.js') }}"></script>

    <script>
    $(function () {

        // Show fields based on choosen battery bank
        $('#new_battery_count').on('change', function(e) {
            var newBatteryCount = $(this).val();
            switch (newBatteryCount) {
                case '4':
                    $(".new_battery_bank_1").show();
                    $(".new_battery_bank_2").hide();
                    $(".new_battery_bank_3").hide();
                    $(".new_battery_bank_4").hide();
                    break;
                case '8':
                    $(".new_battery_bank_1").show();
                    $(".new_battery_bank_2").show();
                    $(".new_battery_bank_3").hide();
                    $(".new_battery_bank_4").hide();
                    break;
                case '12':
                    $(".new_battery_bank_1").show();
                    $(".new_battery_bank_2").show();
                    $(".new_battery_bank_3").show();
                    $(".new_battery_bank_4").hide();
                    break;
                case '16':
                    $(".new_battery_bank_1").show();
                    $(".new_battery_bank_2").show();
                    $(".new_battery_bank_3").show();
                    $(".new_battery_bank_4").show();
                    break;
                default:
                    console.log('Sorry, there\'s no case for '+ newBatteryCount +'.');
            }
            console.log(newBatteryCount);
        });

        // Trigger change on #new_battery_count so it shall display battery fields correctly
        $('#new_battery_count').trigger('change');

        var formData = new FormData();
        const quality = .3;
        const maxWidth = 500;

        $('.input-image').each(function(i, obj) {
            // console.log(i);
            $(this).on('change', function(e) {
                var file = $(this)[0].files[0];
                var paramName = $(this).prop('name');
                var previewImage = $(this).next();
                console.log(paramName);
                console.log(this);

                new ImageCompressor(file, {
                    quality: quality,
                    maxWidth: maxWidth,
                    success(result) {
                        objectURL = URL.createObjectURL(result);
                        previewImage.html("<img style='width:200px' src='" + objectURL + "'/>");
                        formData.append(paramName, result, result.name);
                    }
                });

            });

        });


        // Form Submit
        // document.getElementById('submit').addEventListener('click', (e) => {
        $('#submit_button').on('click', function(e) {
            console.log('Form submitted.');
            // Add text input
            var textData = $('#input_form').serializeArray();
            $.each(textData,function(key,input){
                formData.append(input.name,input.value);
            });
            // console.log(textData);

            // Send the compressed image file to server with XMLHttpRequest.
            axios.post('{{ route('form-data.store') }}', formData)
                .then(function(response) {
                    if (response.data.success) {
                        // alert('Form submitted successfully');
                        console.log('Success');
                        setTimeout(function(){
                            window.location.href = "{{ route('report.index') }}";
                        }, 500)
                    }
                });
        });
    });

    </script>
@endsection

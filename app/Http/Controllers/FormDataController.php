<?php

namespace App\Http\Controllers;


use App\Role;
use App\FormData;


use PDF;
use Uuid;
use Image;
use Carbon\Carbon;
use Jenssegers\Optimus\Optimus;
use Yajra\Datatables\Datatables;


use App\Mail\FormDeleted;
use App\Mail\FormApproved;
use App\Jobs\ReportUpdater;


use Log;
use Auth;
use Mail;
use Illuminate\Http\Request;


class FormDataController extends Controller
{

    public function __construct(Request $request, Optimus $optimus)
    {
        $this->middleware('auth');
        $this->request  = $request;
        $this->optimus  = $optimus;
    }


    public function index()
    {
        // $users = User::all();
        return view('pages.form-data-index');
        // return $users;
    }


    public function getData()
    {
        return Datatables::of(FormData::get())
                    ->whitelist(['staff_name', 'site_name'])
                    ->addColumn('actions', 'layouts.datatables.actions-report')
                    ->rawColumns(['actions'])
                    ->make(true);
    }


    public function show()
    {
        // Get user detail from specified id
        $id         = $this->request->input('id');
        $form_data  = FormData::findOrFail($this->optimus->decode($id));
        return $form_data;
    }


    public function approveData()
    {

        // Assign request
        $request        = $this->request;

        // Check id if valid
        $form_data      = FormData::findOrFail($this->optimus->decode($request->input('id')));

        // Do the approval
        $form_data->is_approved = 1;
        $form_data->approved_at = Carbon::now();
        $form_data->save();

        $parameters     = json_decode($form_data->parameters, true);

        // /*
        // Send notification
        // Notify admin
        $admin_role     = Role::where('name', 'administrator')->first();
        $admin_users    = $admin_role->users;
        $staff          = $form_data->user;
        $when           = Carbon::now()->addSeconds(15);

        $carbon_copies  = ['rpt@gmail.com'];
        if (filter_var($parameters['EMAIL'], FILTER_VALIDATE_EMAIL))
        {
            $carbon_copies[] = $parameters['EMAIL'];
        }

        Mail::to($staff)
            ->cc($carbon_copies)
            ->bcc($admin_users)
            ->later($when, new FormApproved($form_data));
        // */


        // Send the good news
        return back()->with('success', __('messages.form_data_approved_successfully'));
    }



    public function destroy()
    {

        // Assign request
        $request        = $this->request;

        // Check id if valid
        $form_data      = FormData::findOrFail($this->optimus->decode($request->input('id')));

        // // Send notification
        // // Notify admin
        // $admin_role     = Role::where('name', 'administrator')->first();
        // $admin_users    = $admin_role->users;
        // $staff          = $form_data->user;
        // $when           = Carbon::now()->addSeconds(15);
        // Mail::to($staff)
        //     ->bcc($admin_users)
        //     ->later($when, new FormDeleted($form_data));

        // Do the actual deletion
        $form_data->delete();

        // Send the good news
        return back()->with('success', __('messages.form_data_deleted_successfully'));

    }


    public function store()
    {
        $user               = Auth::user();
        $request            = $this->request;

        // Find FormData
        $id                 = $request->input('id');
        $form_data          = FormData::findOrFail($this->optimus->decode($id));
        $parameters         = json_decode($form_data->parameters, true);

        Log::info('[DASHBOARD] Form submission initiation:', ["id" => $this->optimus->decode($id)]);

        // Initialize default value
        $report_data    = [
            'SITE_NAME'         => '',
            'SITE_ID'           => '',
            'SITE_ADDRESS'      => '',
            'SITE_AREA'         => '',
            'NBWO_EWO_NUMBER'   => '',
            'PO_NUMBER'         => '',
            'RESERVATION_NUMBER' => '',
            'INSTALLER_NAME'    => '',
        ];

        $float_data     = [
            'AC_INPUT'          => 220,
            'FLOAT_VOLTAGE'     => 50,
            'EQUALIZE_VOLTAGE'  => 54,
            'VOLTAGE_PER_BLOCK' => 2,
            'LOAD_CURRENT'      => 100,
            'RECTIFIER_SYSTEM_VOLTAGE' => 54,
            'TEMPERATURE'       => 0,
        ];

        $int_data       = [
            'BATTERY_QTY'       => 4,
            'INPUT_PHASE'       => 3,
            'INPUT_FREQUENCY'   => 50,
            'OUTPUT_VOLTAGE_LOW' => 40,
            'OUTPUT_VOLTAGE_HIGH' => 56,
            'ALARM_BATTERY_FUSE_FAIL' => 1,
            'NMM_OSS_POWER'     => 1,
            'DISCHARGE_TIME'    => 60,
            'INSTALL_ON_SAME_RACK' => 1,
        ];


        // Validating parameter type
        foreach ($request->input() as $input_key => $input_value)
        {
            if (in_array($input_key, array_keys($float_data)))
            {
                $report_data[$input_key] = (float) $input_value;
            }

            else if (in_array($input_key, array_keys($int_data)))
            {
                $report_data[$input_key] = (int) $input_value;
            }

            // else if ($input_key == 'OLD_BATTERY')
            // {
            //     $report_data['OLD_BATTERY']['CAGE'] = (int) $input_value['OLD_BATTERY']['CAGE'];
            //     $report_data['OLD_BATTERY']['BRAND'] = $input_value['OLD_BATTERY']['BRAND'];
            //     $report_data['OLD_BATTERY']['QTY'] = (int) $input_value['OLD_BATTERY']['QTY'];
            // }

            else
            {
                $report_data[$input_key] = $input_value;
            }
        }

        if ($request->has('SITE_NAME'))
        {
            $form_data->site_name   = $request->input('SITE_NAME');
        }

        // // Maybe we won't update these values
        // // Logic to get current date
        // $current_date = new Carbon;
        // setlocale(LC_TIME, 'id_ID.utf8');
        // $report_data['ACCEPTANCE_DATE'] = $current_date->formatLocalized('%d %B %Y');
        // $report_data['HARI_ATP']        = $current_date->formatLocalized('%A');
        // $report_data['TANGGAL_ATP']     = $current_date->formatLocalized('%d');
        // $report_data['BULAN_ATP']       = $current_date->formatLocalized('%B');
        // $report_data['TAHUN_ATP']       = $current_date->formatLocalized('%Y');

        $report_data['ACCEPTANCE_DATE'] = $parameters['ACCEPTANCE_DATE'];
        $report_data['HARI_ATP']        = $parameters['HARI_ATP'];
        $report_data['TANGGAL_ATP']     = $parameters['TANGGAL_ATP'];
        $report_data['BULAN_ATP']       = $parameters['BULAN_ATP'];
        $report_data['TAHUN_ATP']       = $parameters['TAHUN_ATP'];

        // Logic to relate each battery type
        switch ($report_data['BATTERY_TYPE'])
        {
            // New battery type: NDT100
            case "NDT_100":
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                $report_data['BATTERY_CAPACITY']    = 100;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512922',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,100AH,NARADA,12ND> NARADA 12NDT100S',
                        'MATERIAL_BRAND'            => 'NARADA/NDT',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER'       => '3413082',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12',
                        'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];
                break;


            case "ICS_150":
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'ICS';
                $report_data['BATTERY_CAPACITY']    = 150;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512280',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:FAST CHARGING,12VDC,150AH,NARAD>',
                        'MATERIAL_BRAND'            => 'NARADA/ICS',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,

                    ],
                    [

                        'SAP_MATERIAL_NUMBER'       => '3413084',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY FAST CHARGE,12V>',
                        'MATERIAL_BRAND'            => 'RAPTEK/ICS',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];
                break;


            case "NDT_150":
            default:
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                $report_data['BATTERY_CAPACITY']    = 150;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512279',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,150AH,NARADA,NDT1>',
                        'MATERIAL_BRAND'            => 'NARADA/NDT',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER'       => '3413083',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12V150AH',
                        'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];

        }


        // Populate BATTERY_SN

        $battery_sns = [];

        foreach ($report_data['NEW_BATTERY'] as $new_battery)
        {
            if ( ! empty($new_battery['SN']) )
            {
                $battery_sns[] = $new_battery['SN'];
            }
        }

        $report_data['BATTERY_SN'] = implode($battery_sns, ', ');


        // Keep old images
        if (isset($parameters['IMAGES']))
        {
            $report_data['IMAGES']  = $parameters['IMAGES'];
        }


        // Process new images
        foreach ($_FILES as $key => $upload_file)
        {
            $image_filename     = Uuid::generate(4).'.jpg';
            $image_path         = 'storage/uploads/'. $image_filename;
            $img                = Image::make($upload_file['tmp_name']);
            $img->save($image_path);

            if ($key == 'OLD_BATTERY_IMAGE')
            {
                $report_data['IMAGES']['OLD_BATTERY']  = $image_path;
            }
            else
            {
                $report_data['IMAGES'][$key]  = $image_path;
            }
        }

        $form_data->parameters      = json_encode($report_data);
        $form_data->save();


        // Dispatch PDF Generator
        $job = new ReportUpdater($form_data);
        dispatch($job);

        Log::info('[DASHBOARD] PDF report updated: ', ["id" => $this->optimus->decode($id)]);


        // Let them know the good news
        $request->session()->flash('success', 'Form saved successfully!');

        return response()->json([
            'success' => TRUE,
            'message' => "Form saved successfully"
        ]);


    }






    public function test_report($id)
    {
        $form_data          = FormData::findOrFail($id);
        $report_data        = json_decode($form_data->parameters, true);

        /*
        $report_data        = [
            // 'CHECKBOX_PASS' => 1,
            'SITE_NAME' => 'Petojo Selatan',
            'SITE_ID' => '08764',
            'SITE_ADDRESS' => 'Jalan Kusumanegara 14',
            'SITE_AREA' => 'Jakarta Pusat',
            'NBWO_EWO_NUMBER' => '2018/asdf/1231',
            'PO_NUMBER' => '2018/23112/12341/asdf/13',
            'RESERVATION_NUMBER' => '2018/aa/zxc/df',
            'INSTALLER_NAME' => 'Dolph Lundgren',
            'OLD_BATTERY' => [
                'CAGE' => 1,
                'BRAND' => 'Sonnenschein',
                'QTY' => 8,
            ],
            'BATTERY_QTY' => 16,

            'AC_INPUT' => 220,
            'INPUT_PHASE' => 3,
            'INPUT_FREQUENCY' => 60,
            'OUTPUT_VOLTAGE_LOW' => 40,
            'OUTPUT_VOLTAGE_HIGH' => 56,
            'FLOAT_VOLTAGE' => 50,
            'EQUALIZE_VOLTAGE' => 54,
            'BATTERY_TYPE' => 'NDT',
            'BATTERY_CAPACITY' => 150,
            'VOLTAGE_PER_BLOCK' => 2,
            'LOAD_CURRENT' => 100,
            'ALARM_BATTERY_FUSE_FAIL' => 1,
            'RECTIFIER_SYSTEM_VOLTAGE' => 54,
            'NMM_OSS_POWER' => 0,
            'DISCHARGE_TIME' => 60,
            'INSTALL_ON_SAME_RACK' => 1,
            'TEMPERATURE' => 45,
            'EMAIL' => 'r00t4bl3@gmail.com',
            'NEW_BATTERY' => [
                'BATT_1' => [
                    'SN' => 'BATT-01',
                    'T1' => '11',
                    'T2' => '12',
                    'T3' => '13',
                    'T4' => '14',
                    'REMARK' => 1,
                ],
                'BATT_2' => [
                    'SN' => 'BATT-02',
                    'T1' => '21',
                    'T2' => '22',
                    'T3' => '23',
                    'T4' => '24',
                    'REMARK' => 0,
                ],
                'BATT_3' => [
                    'SN' => 'BATT-03',
                    'T1' => '31',
                    'T2' => '32',
                    'T3' => '33',
                    'T4' => '34',
                    'REMARK' => 1,
                ],
                'BATT_4' => [
                    'SN' => 'BATT-04',
                    'T1' => '41',
                    'T2' => '42',
                    'T3' => '43',
                    'T4' => '44',
                    'REMARK' => 0,
                ],
                'BATT_5' => [
                    'SN' => 'BATT-05',
                    'T1' => '51',
                    'T2' => '52',
                    'T3' => '53',
                    'T4' => '54',
                    'REMARK' => 1,
                ],
                'BATT_6' => [
                    'SN' => 'BATT-06',
                    'T1' => '61',
                    'T2' => '62',
                    'T3' => '63',
                    'T4' => '64',
                    'REMARK' => 0,
                ],
                'BATT_7' => [
                    'SN' => 'BATT-07',
                    'T1' => '71',
                    'T2' => '72',
                    'T3' => '73',
                    'T4' => '74',
                    'REMARK' => 1,
                ],
                'BATT_8' => [
                    'SN' => 'BATT-08',
                    'T1' => '81',
                    'T2' => '82',
                    'T3' => '83',
                    'T4' => '84',
                    'REMARK' => 0,
                ],
                'BATT_9' => [
                    'SN' => 'BATT-09',
                    'T1' => '91',
                    'T2' => '92',
                    'T3' => '93',
                    'T4' => '94',
                    'REMARK' => 1,
                ],
                'BATT_10' => [
                    'SN' => 'BATT-10',
                    'T1' => '101',
                    'T2' => '102',
                    'T3' => '103',
                    'T4' => '104',
                    'REMARK' => 0,
                ],
                'BATT_11' => [
                    'SN' => 'BATT-11',
                    'T1' => '111',
                    'T2' => '112',
                    'T3' => '113',
                    'T4' => '114',
                    'REMARK' => 1,
                ],
                'BATT_12' => [
                    'SN' => 'BATT-12',
                    'T1' => '121',
                    'T2' => '122',
                    'T3' => '123',
                    'T4' => '124',
                    'REMARK' => 0,
                ],
                'BATT_13' => [
                    'SN' => 'BATT-13',
                    'T1' => '131',
                    'T2' => '132',
                    'T3' => '133',
                    'T4' => '134',
                    'REMARK' => 1,
                ],
                'BATT_14' => [
                    'SN' => 'BATT-14',
                    'T1' => '141',
                    'T2' => '142',
                    'T3' => '143',
                    'T4' => '144',
                    'REMARK' => 0,
                ],
                'BATT_15' => [
                    'SN' => 'BATT-15',
                    'T1' => '151',
                    'T2' => '152',
                    'T3' => '153',
                    'T4' => '154',
                    'REMARK' => 1,
                ],
                'BATT_16' => [
                    'SN' => 'BATT-16',
                    'T1' => '161',
                    'T2' => '162',
                    'T3' => '163',
                    'T4' => '164',
                    'REMARK' => 0,
                ],
            ],


        ];

        // dd (json_encode($report_data));

        // Logic to get current date
        $current_date = new Carbon;
        setlocale(LC_TIME, 'id_ID.utf8');
        $report_data['ACCEPTANCE_DATE'] = $current_date->formatLocalized('%d %B %Y');
        $report_data['HARI_ATP'] = $current_date->formatLocalized('%A');
        $report_data['TANGGAL_ATP'] = $current_date->formatLocalized('%d');
        $report_data['BULAN_ATP'] = $current_date->formatLocalized('%B');
        $report_data['TAHUN_ATP'] = $current_date->formatLocalized('%Y');


        // Logic to relate each battery type
        switch ($report_data['BATTERY_TYPE'])
        {
            case "ICS":
                $report_data['BATTERY_BRAND'] = 'NARADA';
                $report_data['MATERIAL'] = [
                    [
                        'SAP_MATERIAL_NUMBER' => '2512280',
                        'MATERIAL_DESCRIPTION' => 'BATTERY:FAST CHARGING,12VDC,150AH,NARAD>',
                        'MATERIAL_BRAND' => 'NARADA/ICS',
                        'MATERIAL_QTY' => $report_data['BATTERY_QTY'],
                        'RESULT' => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER' => '3413084',
                        'MATERIAL_DESCRIPTION' => 'DEL_INSTALL:FOR BATTERY FAST CHARGE,12V>',
                        'MATERIAL_BRAND' => 'RAPTEK/ICS',
                        'MATERIAL_QTY' => 1,
                        'RESULT' => 1,
                    ]
                ];

                break;

            case "NDT":
            default:
                $report_data['BATTERY_BRAND'] = 'NARADA';
                $report_data['MATERIAL'] = [
                    [
                        'SAP_MATERIAL_NUMBER' => '2512279',
                        'MATERIAL_DESCRIPTION' => 'BATTERY:STORAGE,12VDC,150AH,NARADA,NDT1>',
                        'MATERIAL_BRAND' => 'NARADA/NDT',
                        'MATERIAL_QTY' => $report_data['BATTERY_QTY'],
                        'RESULT' => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER' => '3413083',
                        'MATERIAL_DESCRIPTION' => 'DEL_INSTALL:FOR BATTERY AGM 12V150AH',
                        'MATERIAL_BRAND' => 'RAPTEK/NDT',
                        'MATERIAL_QTY' => 1,
                        'RESULT' => 1,
                    ]
                ];

        }

        // Populate BATTERY_SN
        $battery_sns = [];
        foreach ($report_data['NEW_BATTERY'] as $new_battery)
        {
            if ( ! empty($new_battery['SN']) )
            {
                $battery_sns[] = $new_battery['SN'];
            }
        }

        $report_data['BATTERY_SN'] = implode($battery_sns, ', ');
        */



        // return view('pages.report', compact('report_data'));

        // dd($report_data);


        // /*
        $pdf                = PDF::loadView('pages.report', compact(['report_data']));

        return $pdf->stream();

        // */
    }



    public function test_generate($id)
    {
        $form_data          = FormData::findOrFail($id);

        if ($form_data->report_path)
        {
            $pdf_relative_path  = $form_data->report_path;
        }
        else
        {
            $pdf_relative_path  = 'storage/reports/'.str_slug($form_data->site_name).'_'.str_slug(Carbon::now()->toDateString()).'_'.str_slug(Carbon::now()->toTimeString()).'.pdf';
        }

        $pdf_file_path      = public_path($pdf_relative_path);

        $report_data        = json_decode($form_data->parameters, true);

        // Update database with new battery fields
        switch ($report_data['BATTERY_TYPE'])
        {
            // New battery type: NDT100
            case "NDT_100":
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                $report_data['BATTERY_CAPACITY']    = 100;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512922',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,100AH,NARADA,12ND> NARADA 12NDT100S',
                        'MATERIAL_BRAND'            => 'NARADA/NDT',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER'       => '3413082',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12',
                        'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];

                break;

            // Allow old apps version
            case "ICS_150":
            case "ICS":
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'ICS';
                $report_data['BATTERY_CAPACITY']    = 150;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512280',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:FAST CHARGING,12VDC,150AH,NARAD>',
                        'MATERIAL_BRAND'            => 'NARADA/ICS',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER'       => '3413084',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY FAST CHARGE,12V>',
                        'MATERIAL_BRAND'            => 'RAPTEK/ICS',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];

                break;

            // Allow old apps version
            case "NDT_150":
            case "NDT":
            default:
                $report_data['BATTERY_BRAND']       = 'NARADA';
                $report_data['BATTERY_TYPE_DISPLAY'] = 'NDT';
                $report_data['BATTERY_CAPACITY']    = 150;
                $report_data['MATERIAL']            = [
                    [
                        'SAP_MATERIAL_NUMBER'       => '2512279',
                        'MATERIAL_DESCRIPTION'      => 'BATTERY:STORAGE,12VDC,150AH,NARADA,NDT1>',
                        'MATERIAL_BRAND'            => 'NARADA/NDT',
                        'MATERIAL_QTY'              => $report_data['BATTERY_QTY'],
                        'RESULT'                    => 1,
                    ],
                    [
                        'SAP_MATERIAL_NUMBER'       => '3413083',
                        'MATERIAL_DESCRIPTION'      => 'DEL_INSTALL:FOR BATTERY AGM 12V150AH',
                        'MATERIAL_BRAND'            => 'RAPTEK/NDT',
                        'MATERIAL_QTY'              => 1,
                        'RESULT'                    => 1,
                    ]
                ];

        }

        $form_data->parameters  = json_encode($report_data);
        $form_data->save();


        $pdf                = PDF::loadView('pages.report', compact(['report_data']));

        // Save the PDF to filesystem
        $pdf->save($pdf_file_path);

        // // Update record
        // $form_data->report_path = $pdf_relative_path;
        // $form_data->save();

        return $pdf_file_path;

    }

}
